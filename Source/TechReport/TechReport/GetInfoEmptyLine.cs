﻿using System.Collections.Generic;

namespace TechReport
{
    class GetInfoEmptyLine : IGetInfo
    {
        public string OpName { get { return string.Empty; } }
        public Result GetInfo()
        {
            return new Result(OpName, string.Empty, InfoType.EMPTY_LINE);
        }
    }





    class GetInfoGetEmptyLines : IGetInfo
    {
        private List<string> _emptyLines = new List<string>();
        public string OpName { get { return string.Empty; } }

        public Result GetInfo()
        {
            return new Result(OpName, _emptyLines);
        }

        public GetInfoGetEmptyLines(int count)
        {
            for (int i = 0; i < count; i++)
            {
                _emptyLines.Add(string.Empty);
            }
        }
    }





}
