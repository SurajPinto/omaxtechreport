﻿using System.Collections.Generic;

namespace TechReport
{
    enum InfoType { HEADER, EMPTY_LINE, SINGLE_LINE, MULTI_LINE, MULTI_LINE_KEY_VALUE };
    interface IGetInfo
    {
        string OpName { get; }
        Result GetInfo();
    }




    interface IGetInfoCollection
    {
        List<IGetInfo> GetMethodList();
    }
}
