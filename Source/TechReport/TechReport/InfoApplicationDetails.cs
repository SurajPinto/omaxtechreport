﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using Microsoft.Win32;
using System.Windows.Input;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;

namespace TechReport
{
    class InfoApplicationDetails : IGetInfoCollection
    {
        public List<IGetInfo> GetMethodList()
        {
            List<IGetInfo> methods = new List<IGetInfo>()
            {
                new GetHeader(),
                new GetCurrentOpenFilename(),
                new GetOfficialVersion(),
                new GetPatchNumber(),
                new GetExtendedVersionInfo(),
                new GetIntelliMAXVersionNumberFromInternet(),
                new GetCompileDate(),
                new GetCompileEngineer(),
                new ExeIntegrityCheck(),
                new CodeIntegrityCheck(),
                new GetISUserName(),
                new GetISCompanyName(),
                new GetISSerialNumber(),
                new GetWindowsRegisteredUserName(),
                new GetWindowsRegisteredCompanyName(),
                new GetCurrentlyLoggedOnUser(),
                new GetComputerName(),
                new GetUniqueFingerPrint(),
                new GetSamsonFingerPrint(),
                new GetMacAddress(),
                new HardDiskSerialNumber(),
                new GetIsAdministrator(),
                new GetDellServiceTagInfo(),
                new GetConfidenceComputerWasConfiguredByOMAX(),
                new GetHelpFilePath(),
                new GetApplicationHelpFileName(),
                new GetHTMLOIRName(),
                new GetOIRWindowName(),
                new GetIsRunningFromIDE(),
                new GetIsUSB2DriversAvailable(),
                new GetLocationOfExe(),
                new GetCommandLineParameters(),
                new GetKeyboardStateIsShiftKeyDown(),
                new GetKeyboardStateIsControlKeyDown(),
                new GetKeyboardStateIsAltKeyDown(),
                new GetKeyboardStateIsNumLockOn(),
                new GetKeyboardStateIsCapsLockOn(),
                new GetKeyboardKeyDelay(),
                new GetKeyboardKeySpeed()
            };

            return methods;
        }


        private static bool IsBetaVersion()
        {
            bool betaVersion = false;
            string minorVersion = string.Empty;

            minorVersion = GetVersionInfoFractionalPart();

            if (!string.Equals(minorVersion, "0"))
            {
                betaVersion = true;
            }

            return betaVersion;
        }


        private static string GetVersionInfoFromINI()
        {
            // reads the Version.ini file
            string versionNumber = string.Empty;
            string iniFilename = Path.Combine(Util.FilesAndFolders.GetOMAXSharedFileRootFolder(), @"Version.INI");
            versionNumber = Util.Ini.ReadValue("VersionInfo", "OFFICIAL_RELEASE_VERSION", iniFilename, "");
            return versionNumber;
        }


        private static string GetVersionInfoFractionalPart()
        {
            // reads WinMAKE.exe to get version information
            string majorVersion = string.Empty;
            string minorVersion = "0";

            string AppVersionInfo = GetWinMAKEVersionNumber();
            string[] versionDetails = AppVersionInfo.Split('.');
            if (versionDetails.Length > 0)
                majorVersion = versionDetails[0];

            if (versionDetails.Length > 1)
            {
                int lengthToRemove = versionDetails.Length;
                if ((versionDetails[0].Length + 1) < versionDetails.Length)
                    lengthToRemove = versionDetails[0].Length + 1;
                minorVersion = AppVersionInfo.Remove(0, lengthToRemove);
            }

            return minorVersion;
        }

        private static string GetBetaVersionString()
        {
            string betaVersionString = string.Empty;
            if (IsBetaVersion())
            {
                betaVersionString += ">>>>>>>>>> NOTE: THIS IS UNOFFICIAL BETA TEST SOFTWARE <<<<<<<<<<";
            }

            return betaVersionString;
        }


        private static string GetWinMAKEVersionNumber()
        {
            string versionNumber = string.Empty;
            string path = Util.FilesAndFolders.GetOMAXLayoutAndMakeFolder();
            string appExeName = Path.Combine(path, "WinMAKE.exe");
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(appExeName);
            versionNumber = myFileVersionInfo.FileVersion;
            return versionNumber;
        }










        private class GetHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "Application Details", InfoType.HEADER);
            }
        }


        private class GetCurrentOpenFilename : IGetInfo
        {
            public string OpName { get { return "Current file open"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }

        private class GetOfficialVersion : IGetInfo
        {
            public string OpName { get { return "OFFICIAL SOFTWARE VERSION"; } }
            public Result GetInfo()
            {
                string officalVersionInfo = string.Empty;
                officalVersionInfo = GetVersionInfoFromINI() + GetVersionInfoFractionalPart() + " " + Util.Brand.GetBrandName();
                return new Result(OpName, officalVersionInfo);
            }
        }


        private class GetPatchNumber : IGetInfo
        {
            public string OpName { get { return "Patch"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }


        private class GetExtendedVersionInfo : IGetInfo
        {
            public string OpName { get { return "Extended Version Info."; } }
            public Result GetInfo()
            {
                string extendedVersionInfo = string.Empty;
                extendedVersionInfo = GetWinMAKEVersionNumber() + ". Retrieval of Release and Build Info to be implemented.";
                return new Result(OpName, extendedVersionInfo);
            }
        }


        private class GetIntelliMAXVersionNumberFromInternet : IGetInfo
        {
            public string OpName { get { return "Latest IntelliMAX Version Number."; } }
            public Result GetInfo()
            {
                string returnValue = string.Empty;
                string iniFilename = Path.Combine(Util.FilesAndFolders.GetOMAXSharedFileRootFolder(), @"softwareversions.ini");


                try
                {
                    WebRequest request = WebRequest.Create("http://www.abrasivewaterjet.net/sw_eng/softwareversions.ini");
                    request.Credentials = CredentialCache.DefaultCredentials;
                    WebResponse response = request.GetResponse();
                    //WriteLine(((HttpWebResponse)response).StatusDescription);
                    // Get the stream containing content returned by the server.  
                    Stream dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.  
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.  
                    string responseFromServer = reader.ReadToEnd();
                    // Display the content.  
                    // Console.WriteLine(responseFromServer);
                    // Clean up the streams and the response.  
                    reader.Close();
                    response.Close();

                    File.WriteAllText(iniFilename, responseFromServer);
                    returnValue = Util.Ini.ReadValue("Versions", "Main", iniFilename, "0");
                    File.Delete(iniFilename);

                }
                catch (Exception) { };

                return new Result(OpName, returnValue);
            }
        }


        private class GetCompileDate : IGetInfo
        {
            public string OpName { get { return "Compile Date"; } }
            public Result GetInfo()
            {
                DateTime compileDateTime = DateTime.MinValue;

                string exeFilename = Path.Combine(Util.FilesAndFolders.GetOMAXLayoutAndMakeFolder(), "WinMAKE.exe");
                compileDateTime = File.GetLastWriteTime(exeFilename);
                string compileDate = $"{compileDateTime.Year}/{compileDateTime.Month}/{compileDateTime.Day}";

                return new Result(OpName, compileDate.ToString());
            }
        }


        private class GetCompileEngineer : IGetInfo
        {
            public string OpName { get { return "Compile Engineer"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }



        private class ExeIntegrityCheck : IGetInfo
        {
            public string OpName { get { return "EXE Integrity Check"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }


        private class CodeIntegrityCheck : IGetInfo
        {
            public string OpName { get { return "Code Integrity Check"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }


        private class GetISUserName : IGetInfo
        {
            public string OpName { get { return "Username"; } }
            public Result GetInfo()
            {
                string regKey = @"SOFTWARE\OMAX Corporation\OMAX_LAYOUT_and_MAKE\Windows Version";
                string regValueKey = @"Name";
                string defaultValue = "Unknown";
                string username = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.LocalMachine, regKey, regValueKey, defaultValue);
                return new Result(OpName, username);
            }
        }



        private class GetISCompanyName : IGetInfo
        {
            public string OpName { get { return "Company"; } }
            public Result GetInfo()
            {
                string regKey = @"SOFTWARE\OMAX Corporation\OMAX_LAYOUT_and_MAKE\Windows Version";
                string regValueKey = @"Company";
                string defaultValue = "Unknown";
                string companyName = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.LocalMachine, regKey, regValueKey, defaultValue);
                return new Result(OpName, companyName);
            }
        }



        private class GetISSerialNumber : IGetInfo
        {
            public string OpName { get { return "Serial Number"; } }
            public Result GetInfo()
            {
                string regKey = @"SOFTWARE\OMAX Corporation\OMAX_LAYOUT_and_MAKE\Windows Version";
                string regValueKey = @"Serial";
                string defaultValue = "Unknown";
                string serialNumber = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.LocalMachine, regKey, regValueKey, defaultValue);
                return new Result(OpName, serialNumber);
            }
        }



        private class GetWindowsRegisteredUserName : IGetInfo
        {
            public string OpName { get { return "Windows Registered Username"; } }
            public Result GetInfo()
            {
                string regKey = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion";
                string regValueKey = @"RegisteredOwner";
                string defaultValue = "Unknown";
                string windowsRegUserName = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.LocalMachine, regKey, regValueKey, defaultValue);
                return new Result(OpName, windowsRegUserName);
            }
        }


        private class GetWindowsRegisteredCompanyName : IGetInfo
        {
            public string OpName { get { return "Windows Registered Company Name"; } }
            public Result GetInfo()
            {
                string regKey = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion";
                string regValueKey = @"RegisteredOrganization";
                string defaultValue = "Unknown";
                string windowsRegCompanyName = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.LocalMachine, regKey, regValueKey, defaultValue);
                return new Result(OpName, windowsRegCompanyName);
            }
        }


        private class GetCurrentlyLoggedOnUser : IGetInfo
        {
            public string OpName { get { return "Currently logged on user"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Environment.UserName);
            }
        }


        private class GetComputerName : IGetInfo
        {
            public string OpName { get { return "Computer Name"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Environment.MachineName);
            }
        }


        private class GetUniqueFingerPrint : IGetInfo
        {
            public string OpName { get { return "Yellow Code"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }


        private class GetSamsonFingerPrint : IGetInfo
        {
            public string OpName { get { return "Green Code"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }


        private class GetMacAddress : IGetInfo
        {
            public string OpName { get { return "MAC Address"; } }
            public Result GetInfo()
            {
                string macAddress = string.Empty;

                NetworkInterface[] interfaces = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface i in interfaces)
                {
                    if (i.NetworkInterfaceType == NetworkInterfaceType.Tunnel || i.NetworkInterfaceType == NetworkInterfaceType.Loopback)
                        continue;
                    if (i.Description.ToLower().Contains("virtual"))
                        continue;

                    if (i.OperationalStatus == OperationalStatus.Up)
                    {
                        IPInterfaceStatistics ipStats = i.GetIPStatistics();
                        IPv4InterfaceStatistics ipv4Stats = i.GetIPv4Statistics();
                        if ((ipStats.BytesReceived > 0 && ipStats.BytesSent > 0) || (ipv4Stats.BytesReceived > 0) && (ipv4Stats.BytesSent > 0))
                        {
                            byte[] addrBytes = i.GetPhysicalAddress().GetAddressBytes();
                            if (addrBytes.Length > 0)
                            {
                                macAddress = addrBytes[0].ToString("X2");
                                for (int j = 1; j < addrBytes.Length; j++)
                                {
                                    macAddress += $":{addrBytes[j].ToString("X2")}";
                                }
                            }
                        }
                    }
                }

                return new Result(OpName, macAddress);
            }
        }



        private class HardDiskSerialNumber : IGetInfo
        {
            [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
            private static extern bool GetVolumeInformation(string Volume, StringBuilder VolumeName,
                                                uint VolumeNameSize, out uint SerialNumber, out uint SerialNumberLength,
                                                out uint flags, StringBuilder fs, uint fs_size);
            public string OpName { get { return "Yellow Code"; } }
            public Result GetInfo()
            {
                string hdSerialNumber = string.Empty;


                StringBuilder volumename = new StringBuilder(256);
                StringBuilder fstype = new StringBuilder(256);
                string drives = "C:\\";
                string lblVolume = string.Empty;

                bool ok = GetVolumeInformation(drives, volumename, (uint)volumename.Capacity - 1,
                                    out uint serialNum, out uint serialNumLength, out uint flags, fstype, (uint)fstype.Capacity - 1);
                if (ok)
                {
                    hdSerialNumber = (serialNum != 0) ? serialNum.ToString() : "Unknown";
                }

                return new Result(OpName, hdSerialNumber);
            }
        }


        private class GetIsAdministrator : IGetInfo
        {
            public string OpName { get { return "Is Administrator"; } }
            public Result GetInfo()
            {
                /* An account that has admin priviliges does not show as admin,
                 * so below method is not accurate.
                WindowsIdentity  identity = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal =  new WindowsPrincipal (identity);
                bool isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
                */
                return new Result(OpName, "### To be implemented ###");
            }
        }


        private class GetDellServiceTagInfo : IGetInfo
        {
            public string OpName { get { return "Dell Serivce Tag"; } }
            public Result GetInfo()
            {
                string regKey = @"SOFTWARE\Dell Computer Corporation\SysInfon";
                string regValueKey = @"SerialNumber";
                string defaultValue = "";
                string dellSerialNumber = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.CurrentUser, regKey, regValueKey, defaultValue);

                if (string.IsNullOrEmpty(dellSerialNumber))
                {
                    regKey = @"Software\Microsoft\Windows\ShellNoRoam";
                    regValueKey = string.Empty;
                    dellSerialNumber = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.CurrentUser, regKey, regValueKey, defaultValue);
                }

                return new Result(OpName, dellSerialNumber);
            }
        }

        /// <summary>
        /// This ia porting of the Delphi implementation of this functionality
        /// 
        /// This returns a confidence level as to how likely it is that the
        /// computer running was purchased through and configured by OMAX. (As
        /// opposed to an offline PC, or a PC that the user bought and configured
        /// themselves, which OMAX does not support.)
        ///
        /// 100%  = For sure configured by OMAX / purchased through OMAX
        /// 1-99% = An imperfect confidence level
        /// 0%   = No way OMAX configured this computer
        /// -1   = We can't tell either way, or the question is not valid (For example,
        /// this is an office PC.)
        ///
        /// Note: Other than the condition where we read a special flag from the registry,
        /// we really don't have a good way to know for sure, so please don't over-
        /// rely on the accuracy of the results here.
        /// </summary>
        /// <returns></returns>
        private class GetConfidenceComputerWasConfiguredByOMAX : IGetInfo
        {
            public string OpName { get { return "Factory Config Confidence"; } }
            public Result GetInfo()
            {
                int result = 0;
                string resultStr = string.Empty;

                int cachedResult = -2;
                bool tempBool = false;
                uint configuredAtFacility = 0;

                try
                {

                    if (cachedResult == -2)
                    {
                        result = 0;
                        try
                        {
                            resultStr = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.LocalMachine, @"SYSTEM\CustomConfig", @"ConfiguredAtFacility", string.Empty, true);
                            uint.TryParse(resultStr, out configuredAtFacility);
                        }
                        catch (Exception)
                        {
                            configuredAtFacility = 0;
                        }

                        // if it's a "1", then we are certain it is an OMAX configured machine,
                        if (configuredAtFacility == 1)
                        {
                            // absolutely certain
                            result = 100;
                            cachedResult = result;
                            // goes to end of function
                        }
                        //  otherwise, we test some other things with less certainty.}
                        else
                        {
                            // check if we are configured as an OMAX tool, if not then return  a -1
                            string iniValue = Util.Ini.ReadValue(@"COMMUNICATION", @"ConnectedToOMAX", Util.FilesAndFolders.GetMakePreferencesINIFilename(), tempBool.ToString());
                            bool successReadingIniValue = bool.TryParse(iniValue, out bool connectedToOMAX);

                            // success reading Ini value
                            if (successReadingIniValue == true)
                            {
                                // Not connected to an OMAX machine so set result to -1
                                if (connectedToOMAX == false)
                                {
                                    result = -1;
                                    cachedResult = result;
                                    // goes to end of function (nothing else needs to be done)
                                }
                                // We are connected to an OMAX machine, so configured as machine tool
                                else
                                {
                                    // If we got to this point, then we don't really know much, other than
                                    // we are configured as a machine tool.  So, now we do some heuristics
                                    // to figure out roughly how likley it is we are actually running a
                                    // computer sold and configured by OMAX.We are pretty much guessing at this point

                                    // check against computer name
                                    string computerName = new GetComputerName().GetInfo().GetResultStr().Trim().ToUpper();
                                    if (string.Equals(computerName, "OMAX-CONTROL-PC") || string.Equals(computerName, "MAXIEMCONTROLPC"))
                                        result += 25;

                                    // check against user name
                                    string userName = new GetCurrentlyLoggedOnUser().GetInfo().GetResultStr().Trim().ToUpper();
                                    if (string.Equals(userName, "OMAX CUSTOMER") || string.Equals(userName, "MAXIEMCUSTOMER"))
                                        result += 25;

                                    // if this is a Dell, then we are a little bit more confident this was an OMAX configured compter
                                    if (!string.IsNullOrEmpty(new GetDellServiceTagInfo().GetInfo().GetResultStr().TrimEnd()))
                                        result += 10;
                                }
                            }
                            // Unable to read the ini file, so we can't be sure of anything, so exit
                            else
                            {
                                result = -1;
                                cachedResult = result;
                                // goes to end of function (nothing else needs to be done)

                            }

                            // Don't be over confident!  Limit to 99% confidence.  By limiting to 99%
                            // confidence here, we can flag the user of this function that there is
                            // indeed some uncertainty, even if all the evidence looks good.}
                            if (result > 99)
                                result = 99;
                        }

                        // remember this result for the next time
                        cachedResult = result;
                    }
                    else
                    {
                        result = cachedResult;
                    }
                }
                catch
                {
                    result = -1;
                }

                return new Result(OpName, result.ToString());
            }
        }


        private class GetHelpFilePath : IGetInfo
        {
            public string OpName { get { return "Help File Path"; } }
            public Result GetInfo()
            {
                /* Delphi code for getting help file path in Function GetHelpFileName:String;
                    {Get just the language and locale information}
                    TempLanguage:=ExtractAfterColon(MultilizerLanguage);
                    TempLocale:=ExtractAfterColon(MultilizerLocale);

                    {Get the base path for the application.}
                    TempStr:=ExtractFilePath(ParamStr(0));
                    {add directory to the help of our language.}
                    TempStr:=TempStr+'OMAX_Reference\';
                    {add name of our .exe, and change extension to .hlp}
                    TempStr:=TempStr+ChangeFileExt(ExtractFileName(ParamStr(0)),'.HLP");
                    //TempStr:=TempStr+'OMAX INTERACTIVE REFERENCE.HLP';//@@@ Force to OIR
                    {Return new path for the help system.}
                    Result:=TempStr;
                */
                return new Result(OpName, @"To be implemented. (MAKE JSON OBJECT)  Multilizer Language Related. dir\OMAX_Reference\ExtractFilename.hlp");
            }
        }


        private class GetApplicationHelpFileName : IGetInfo
        {
            public string OpName { get { return "Application Help Filename"; } }
            public Result GetInfo()
            {
                return new Result(OpName, @"To be implemented. (MAKE JSON OBJECT)  Multilizer Language Related. dir\OMAX_Reference\ExtractFilename.hlp");
            }
        }


        private class GetHTMLOIRName : IGetInfo
        {
            public string OpName { get { return "HTML OIR Name"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT)  Multilizer Language Related");
            }
        }


        private class GetOIRWindowName : IGetInfo
        {
            public string OpName { get { return "OIR Window Name"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }


        private class GetIsRunningFromIDE : IGetInfo
        {
            public string OpName { get { return "Running in IDE"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "### To be implemented ###");
            }
        }


        private class GetIsUSB2DriversAvailable : IGetInfo
        {
            public string OpName { get { return "USB 2.0 Drivers Availble"; } }
            public Result GetInfo()
            {
                bool usb2DriversAvailable = false;
                string keyName = "Name";
                string keyPathName = "PathName";
                string usb2DriverName = "usbehci.sys";
                List<Dictionary<string, string>> listDrivers = UtilSysMgmt.GetAllResults(UtilSysMgmt.SYS_MANAGEMENT.Win32_SystemDriver.ToString());
                foreach (Dictionary<string, string> dict in listDrivers)
                {
                    if (dict.ContainsKey(keyName) && dict.ContainsKey(keyPathName))
                    {
                        string driverFilename = Path.GetFileName(dict[keyPathName]).ToLower();
                        if (string.Equals(driverFilename, usb2DriverName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            usb2DriversAvailable = true;
                            break;
                        }
                    }
                }

                return new Result(OpName, usb2DriversAvailable.ToString());
            }
        }



        private class GetLocationOfExe : IGetInfo
        {
            public string OpName { get { return "Location of EXE"; } }
            public Result GetInfo()
            {
                // Path.Combine(Util.FilesAndFolders.GetOMAXLayoutAndMakeFolder(), "WinMAKE.exe")
                // find exe's WinMAKE.exe and LAYOUT.exe running, find path of these processes
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT). Or get path of WinMAKE & LAYOUT processes executing");
            }
        }


        private class GetCommandLineParameters : IGetInfo
        {
            public string OpName { get { return "Command Line Parameters"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT).");
            }
        }



        private class GetKeyboardStateIsShiftKeyDown : IGetInfo
        {
            public string OpName { get { return "Shift Key Down"; } }
            public InfoType GetResultType { get { return InfoType.SINGLE_LINE; } }

            public Result GetInfo()
            {
                string shiftKeyDown = "False";
                if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                    shiftKeyDown = "True";
                return new Result(OpName, shiftKeyDown);
            }
        }



        private class GetKeyboardStateIsControlKeyDown : IGetInfo
        {
            public string OpName { get { return "Control Key Down"; } }
            public Result GetInfo()
            {
                string controlKeyDown = "False";
                if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                    controlKeyDown = "True";
                return new Result(OpName, controlKeyDown);
            }
        }



        private class GetKeyboardStateIsAltKeyDown : IGetInfo
        {
            public string OpName { get { return "Alt Key Down"; } }
            public Result GetInfo()
            {
                string altKeyDown = "False";
                if (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt))
                    altKeyDown = "True";
                return new Result(OpName, altKeyDown);
            }
        }



        private class GetKeyboardStateIsNumLockOn : IGetInfo
        {
            public string OpName { get { return "Num Lock On"; } }
            public Result GetInfo()
            {
                string numLockOn = "False";
                if (Keyboard.IsKeyToggled(Key.NumLock))
                    numLockOn = "True";
                return new Result(OpName, numLockOn);
            }
        }



        private class GetKeyboardStateIsCapsLockOn : IGetInfo
        {
            public string OpName { get { return "Caps Lock On"; } }
            public Result GetInfo()
            {
                string capsLockOn = "False";
                if (Keyboard.IsKeyToggled(Key.CapsLock))
                    capsLockOn = "True";
                return new Result(OpName, capsLockOn);
            }
        }



        private class GetKeyboardKeyDelay : IGetInfo
        {
            public string OpName { get { return "Key Delay"; } }
            public Result GetInfo()
            {
                string keyDelay = "0";
                string propertyName = "KeyboardDelay";
                keyDelay = Util.SystemInformation.GetSystemInformationPropertyValue(propertyName);
                return new Result(OpName, keyDelay);
            }
        }



        private class GetKeyboardKeySpeed : IGetInfo
        {
            public string OpName { get { return "Key Speed"; } }
            public Result GetInfo()
            {
                string keySpeed = "0";
                string propertyName = "KeyboardSpeed";
                keySpeed = Util.SystemInformation.GetSystemInformationPropertyValue(propertyName);
                return new Result(OpName, keySpeed);
            }
        }





    }
}
