﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace TechReport
{
    class InfoEnvironment : IGetInfoCollection
    {
        public List<IGetInfo> GetMethodList()
        {
            List<IGetInfo> methods = new List<IGetInfo>()
            {
                new GetHeader(),
                new Get_CSIDL_Desktop(),
                new Get_CSIDL_Internet(),
                new Get_CSIDL_Programs(),
                new Get_CSIDL_Controls(),
                new Get_CSIDL_Printers(),
                new Get_CSIDL_Personal(),
                new Get_CSIDL_Favorites(),
                new Get_CSIDL_Startup(),
                new Get_CSIDL_Recent(),
                new Get_CSIDL_SendTo(),
                new Get_CSIDL_BitBucket(),
                new Get_CSIDL_StartMenu(),
                new Get_CSIDL_DesktopDirectory(),
                new Get_CSIDL_Drives(),
                new Get_CSIDL_Network(),
                new Get_CSIDL_NetHood(),
                new Get_CSIDL_Fonts(),
                new Get_CSIDL_Templates(),
                new Get_CSIDL_Common_StartMenu(),
                new Get_CSIDL_Common_Programs(),
                new Get_CSIDL_Common_Startup(),
                new Get_CSIDL_DesktopDirectory(),
                new Get_CSIDL_AppData(),
                new Get_CSIDL_PrintHood(),
                new Get_CSIDL_Local_AppData(),
                new Get_CSIDL_Alt_Startup(),
                new Get_CSIDL_Common_AltStartup(),
                new Get_CSIDL_Common_Favorites(),
                new Get_CSIDL_InternetCache(),
                new Get_CSIDL_Cookies(),
                new Get_CSIDL_History(),
                new Get_CSIDL_Profile(),
                new Get_CSIDL_Connections(),
                new Get_CSIDL_Common_Music(),
                new Get_CSIDL_Common_Pictures(),
                new Get_CSIDL_Common_Video(),
                new Get_CSIDL_CDBurn_Area(),
                new Get_CSIDL_ComputersNearMe(),
                new GetProgramFilesDirectory(),
                new GetWindowsDirectory(),
                new GetTempDirectory(),
                new GetWindowsSystemDirectory(),
                new GetAllUsersAppDataDirectory(),
                new GetCurrentUsersAppDataDirectory(),
                new GetEnvironmentVariablePublic(),
                new GetEnvironmentVariableShared(),
                new GetEnvironmentVariableProcessorRevision(),
                new GetEnvironmentVariableTemp(),
                new GetEnvironmentVariableTmp(),
                new GetOMAXSharedFileRootFolder()
            };

            return methods;
        }






        private class GetHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }

            public Result GetInfo()
            {
                return new Result(OpName, "Environment Details", InfoType.HEADER);
            }
        }


        private class Get_CSIDL_Desktop : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_DESKTOP.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
            }
        }

        private class Get_CSIDL_Internet : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_INTERNET.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_INTERNET));
            }
        }

        private class Get_CSIDL_Programs : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_PROGRAMS.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.Programs));
            }
        }

        private class Get_CSIDL_Controls : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_CONTROLS.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_CONTROLS));
            }
        }

        private class Get_CSIDL_Printers : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_PRINTERS.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_PRINTERS));
            }
        }

        private class Get_CSIDL_Personal : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_PERSONAL.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.Personal));
            }
        }

        private class Get_CSIDL_Favorites : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_FAVORITES.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.Favorites));
            }
        }

        private class Get_CSIDL_Startup : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_STARTUP.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.Startup));
            }
        }

        private class Get_CSIDL_Recent : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_RECENT.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.Recent));
            }
        }

        private class Get_CSIDL_SendTo : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_SENDTO.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.SendTo));
            }
        }

        private class Get_CSIDL_BitBucket : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_BITBUCKET.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_BITBUCKET));
            }
        }

        private class Get_CSIDL_StartMenu : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_STARTMENU.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.StartMenu));
            }
        }

        private class Get_CSIDL_DesktopDirectory : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_DESKTOPDIRECTORY.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
            }
        }

        private class Get_CSIDL_Drives : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_DRIVES.ToString(); } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_DRIVES));
            }
        }

        private class Get_CSIDL_Network : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_NETWORK.ToString(); } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_NETWORK));
            }
        }

        private class Get_CSIDL_NetHood : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_NETHOOD.ToString(); } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_NETHOOD));
            }
        }


        private class Get_CSIDL_Fonts : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_FONTS.ToString(); } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_FONTS));
            }
        }

        private class Get_CSIDL_Templates : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_TEMPLATES.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.Templates));
            }
        }

        private class Get_CSIDL_Common_StartMenu : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_STARTMENU.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu));
            }
        }

        private class Get_CSIDL_Common_Programs : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_PROGRAMS.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms));
            }
        }

        private class Get_CSIDL_Common_Startup : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_STARTUP.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.CommonStartup));
            }
        }

        private class Get_CSIDL_Common_DesktopDirectory : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_DESKTOPDIRECTORY.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory));
            }
        }

        private class Get_CSIDL_AppData : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_APPDATA.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            }
        }

        private class Get_CSIDL_PrintHood : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_PRINTHOOD.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_PRINTHOOD));
            }
        }

        private class Get_CSIDL_Local_AppData : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_LOCAL_APPDATA.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            }
        }

        private class Get_CSIDL_Alt_Startup : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_ALTSTARTUP.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_ALTSTARTUP));
            }
        }

        private class Get_CSIDL_Common_AltStartup : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_ALTSTARTUP.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_COMMON_ALTSTARTUP));
            }
        }

        private class Get_CSIDL_Common_Favorites : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_FAVORITES.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_COMMON_FAVORITES));
            }
        }

        private class Get_CSIDL_InternetCache : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_INTERNET_CACHE.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.InternetCache));
            }
        }

        private class Get_CSIDL_Cookies : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COOKIES.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.Cookies));
            }
        }

        private class Get_CSIDL_History : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_HISTORY.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.History));
            }
        }

        private class Get_CSIDL_Profile : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_PROFILE.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_PROFILE));
            }
        }

        private class Get_CSIDL_Connections : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_CONNECTIONS.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_CONNECTIONS));
            }
        }

        private class Get_CSIDL_Common_Music : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_MUSIC.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.CommonMusic));
            }
        }

        private class Get_CSIDL_Common_Pictures : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_PICTURES.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.CommonPictures));
            }
        }

        private class Get_CSIDL_Common_Video : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_COMMON_VIDEO.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.CommonVideos));
            }
        }

        private class Get_CSIDL_CDBurn_Area : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_CDBURN_AREA.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.CDBurning));
            }
        }

        private class Get_CSIDL_ComputersNearMe : IGetInfo
        {
            public string OpName { get { return Util.Shell.CSIDL.CSIDL_APPDATA.ToString(); } }

            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_APPDATA));
            }
        }

        private class GetProgramFilesDirectory : IGetInfo
        {
            public string OpName { get { return "Program Files Directory"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName));
            }
        }

        private class GetWindowsDirectory : IGetInfo
        {
            public string OpName { get { return "Windows Directory"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.Windows));
            }
        }

        private class GetTempDirectory : IGetInfo
        {
            public string OpName { get { return "GetTempDir"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Path.GetTempPath());
            }
        }

        private class GetWindowsSystemDirectory : IGetInfo
        {
            public string OpName { get { return "Windows System Directory"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Environment.GetFolderPath(Environment.SpecialFolder.System));
            }
        }

        private class GetAllUsersAppDataDirectory : IGetInfo
        {
            public string OpName { get { return "GetAllUsersAppDataDirectory"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_COMMON_APPDATA));
            }
        }

        private class GetCurrentUsersAppDataDirectory : IGetInfo
        {
            public string OpName { get { return "GetCurrentUsersAppDataDirectory"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.Shell.GetSpecialFolderPath(Util.Shell.CSIDL.CSIDL_APPDATA));
            }
        }

        private class GetEnvironmentVariablePublic : IGetInfo
        {
            public string OpName { get { return "GetEnvironmentVariable(public)"; } }
            public Result GetInfo()
            {
                string path = Environment.GetEnvironmentVariable("Public");
                if (string.IsNullOrEmpty(path))
                    path = string.Empty;
                return new Result(OpName, path);
            }
        }

        private class GetEnvironmentVariableShared : IGetInfo
        {
            public string OpName { get { return "GetEnvironmentVariable(Shared)"; } }
            public Result GetInfo()
            {
                string path = Environment.GetEnvironmentVariable("Shared");
                if (string.IsNullOrEmpty(path))
                    path = string.Empty;
                return new Result(OpName, path);
            }
        }

        private class GetEnvironmentVariableProcessorRevision : IGetInfo
        {
            public string OpName { get { return "GetEnvironmentVariable(ProcessorRevision)"; } }
            public Result GetInfo()
            {
                string envVarValue = Environment.GetEnvironmentVariable("PROCESSOR_REVISION");
                if (string.IsNullOrEmpty(envVarValue))
                    envVarValue = string.Empty;
                return new Result(OpName, envVarValue);
            }
        }

        private class GetEnvironmentVariableTemp : IGetInfo
        {
            public string OpName { get { return "GetEnvironmentVariable(Temp)"; } }
            public Result GetInfo()
            {
                string path = Environment.GetEnvironmentVariable("Temp");
                if (string.IsNullOrEmpty(path))
                    path = string.Empty;
                return new Result(OpName, path);
            }
        }

        private class GetEnvironmentVariableTmp : IGetInfo
        {
            public string OpName { get { return "GetEnvironmentVariable(Tmp)"; } }
            public Result GetInfo()
            {
                string path = Environment.GetEnvironmentVariable("Tmp");
                if (string.IsNullOrEmpty(path))
                    path = string.Empty;
                return new Result(OpName, path);
            }
        }

        private class GetOMAXSharedFileRootFolder : IGetInfo
        {
            public string OpName { get { return "GetOMAXSharedFileRootFolder"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.FilesAndFolders.GetOMAXSharedFileRootFolder());
            }
        }
    }
}
