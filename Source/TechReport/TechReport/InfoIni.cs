﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TechReport
{
    class InfoIni : IGetInfoCollection
    {
        public List<IGetInfo> GetMethodList()
        {
            List<IGetInfo> methods = new List<IGetInfo>()
            {
                new GetHeader(),
                new GetInfoEmptyLine(),
                new GetResultOfValidityChecksForIni(),
                new GetInfoGetEmptyLines(3),
                new GetLayoutPreferenceIniHeader(),
                new GetIniInfoText(),
                new GetLayoutPreferencesIni(),
                new GetInfoGetEmptyLines(3),
                new GetMakePreferenceIniHeader(),
                new GetIniInfoText(),
                new GetMakePreferencesIni()
            };

            return methods;
        }



        private class GetHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "Validity Checks for INI Files (Looks for SOME kinds of corruption)", InfoType.HEADER);
            }
        }



        private class GetResultOfValidityChecksForIni : IGetInfo
        {
            private string FormatLHS(string filename, int lineNo, string lineText)
            {
                string formattedText = $"{Path.GetFileName(filename)} lineNo.{lineNo,3} ->   {lineText}     ";
                return formattedText;
            }
            private List<KeyValuePair<string, string>> ProcessIniFile(string filename)
            {
                List<KeyValuePair<string, string>> errors = new List<KeyValuePair<string, string>>();
                string error = string.Empty;
                string[] lines = File.ReadAllLines(filename);
                int lineNo = 0;
                foreach (string line in lines)
                {
                    lineNo++;
                    string l = line.Trim();

                    // empty line or comments to be ignored
                    if (string.IsNullOrEmpty(l) || l.StartsWith(";"))
                    {
                        continue;
                    }
                    // Starts with [ and Ends with ],  so text between square brackets should be valid section text
                    else if (l.StartsWith("[") && l.EndsWith("]"))
                    {
                        if (l.Length > 2)
                        {
                            if (l.Substring(1).Contains("["))
                            {
                                errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Section contains multiple open ["));
                            }

                            if (l.Substring(0, l.Length - 1).Contains("]"))
                            {
                                errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Section contains multiple closing ]"));
                            }

                            string text = l.Substring(1, l.Length - 2).Trim();
                            if (string.IsNullOrEmpty(text))
                            {
                                errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Section header is empty"));
                            }

                            bool containsText = false;
                            foreach (char c in text)
                            {
                                if (Char.IsLetterOrDigit(c))
                                {
                                    containsText = true;
                                    break;
                                }
                            }
                            if (containsText == false)
                            {
                                errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Section header does not contain valid text"));
                            }
                        }
                        else
                        {
                            errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Section header is empty"));
                        }
                    }
                    // Incomplete section header
                    else if ((l.StartsWith("[")) || (l.EndsWith("]")))
                    {
                        if (!l.StartsWith("["))
                        {
                            errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Section does not start with ["));
                        }

                        if (!l.EndsWith("]"))
                        {
                            errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Section does not end with ]"));
                        }

                    }
                    // not a valid line as no = character present 
                    else if (!l.Contains("="))
                    {
                        errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Not a valid section header nor a valid key value entry"));
                    }
                    // Invalid line as LHS of KeyValuePair missing
                    else if (l.StartsWith("="))
                    {
                        errors.Add(new KeyValuePair<string, string>(FormatLHS(filename, lineNo, l), "Missing LHS of KeyValuePair, starts with ="));
                    }
                    // Invalid line as RHS of KeyValuePair missing
                    else if (l.EndsWith("="))
                    {
                        // errors.Add(new KeyValuePair<string, string>($"{lineNumber}: {l}", "Missing RHS of KeyValuePair, ends with ="));
                    }
                    // Invalid ine because there are multiple =
                    else if (l.Split('=').Length > 2)
                    {
                        // errors.Add(new KeyValuePair<string, string>(formatLHS(filename, lineNo, l), "Invalid KeyValuePair  multiple = "));
                    }

                }


                return errors;
            }
            public string OpName { get { return "Validity Checks for Ini file"; } }

            public Result GetInfo()
            {
                List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
                List<KeyValuePair<string, string>> errors = new List<KeyValuePair<string, string>>();
                string publicAllUserDataDirectory = Util.FilesAndFolders.GetOMAXSharedFileRootFolder();
                string[] iniFilenames = Directory.GetFiles(publicAllUserDataDirectory, "*.ini");
                string iniPasswordFilename = "Passwords.ini";

                foreach (string iniFilename in iniFilenames)
                {
                    // if password.ini file then do nothing, insert only a blank line
                    if (string.Equals(Path.GetFileName(iniFilename), iniPasswordFilename, StringComparison.InvariantCultureIgnoreCase))
                    {
                        result.Add(new KeyValuePair<string, string>(string.Empty, string.Empty));
                        continue;
                    }

                    List<KeyValuePair<string, string>> e = ProcessIniFile(iniFilename);
                    if (e.Count == 0)
                    {
                        result.Add(new KeyValuePair<string, string>(iniFilename, "OK"));
                    }
                    else
                    {
                        errors.AddRange(e);
                    }
                }

                result.AddRange(errors);

                return new Result(OpName, result);
            }
        }



        private class GetLayoutPreferenceIniHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }

            public Result GetInfo()
            {
                string result = $"INI File settings for \"{Util.FilesAndFolders.GetLayoutPreferencesINIFilename()}\"";
                return new Result(OpName, result, InfoType.HEADER);
            }
        }



        private class GetLayoutPreferencesIni : IGetInfo
        {
            public string OpName { get { return Util.FilesAndFolders.GetLayoutPreferencesINIFilename(); } }

            public Result GetInfo()
            {
                string iniFilename = Path.Combine(Util.FilesAndFolders.GetOMAXSharedFileRootFolder(), Util.FilesAndFolders.GetLayoutPreferencesINIFilename());
                List<string> result = new List<string>(File.ReadAllLines(iniFilename));
                return new Result(OpName, result);
            }
        }




        private class GetMakePreferenceIniHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }

            public Result GetInfo()
            {
                string result = $"INI File settings for \"{Util.FilesAndFolders.GetMakePreferencesINIFilename()}\"";
                return new Result(OpName, result, InfoType.HEADER);
            }
        }




        private class GetMakePreferencesIni : IGetInfo
        {
            public string OpName { get { return Util.FilesAndFolders.GetMakePreferencesINIFilename(); } }

            public Result GetInfo()
            {
                string iniFilename = Path.Combine(Util.FilesAndFolders.GetOMAXSharedFileRootFolder(), Util.FilesAndFolders.GetMakePreferencesINIFilename());
                List<string> result = new List<string>(File.ReadAllLines(iniFilename));
                return new Result(OpName, result);
            }
        }




        private class GetIniInfoText : IGetInfo
        {
            public string OpName { get { return "TEXT"; } }

            public Result GetInfo()
            {
                List<string> result = new List<string>()
                {
                    "",
                    "PLEASE NOTE: The settings displayed here are not necessarily",
                    "the current settings in the software. The INI file might not",
                    "be current.The INI file is typically updated when the user",
                    "exits the software, or after certain dialogs are closed.",
                    ""
                 };

                return new Result(OpName, result);
            }
        }






    }
}
