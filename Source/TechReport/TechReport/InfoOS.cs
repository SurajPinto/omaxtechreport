﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace TechReport
{
    class InfoOS : IGetInfoCollection
    {
        public List<IGetInfo> GetMethodList()
        {
            List<IGetInfo> methods = new List<IGetInfo>()
            {
                new GetHeader(),
                new IsUACEnabled(),
                new GetOSBuildNumber(),
                new GetOSMajor(),
                new GetOSMinor(),
                new GetOSName(),
                new GetOSServicePackMajorVersion(),
                new GetOSServicePackMinorVersion(),
                new GetCompatabilityMode(),
                new GetIsRunningUnderBootcamp(),
                new GetIsRunningUnderVirtualMachine(),
                new GetOSLanguageName(),
                new GetOSLanguageLCID(),
                new GetOSPrimaryLanguageID(),
                new GetOSSubLanguageID(),
                new GetOSFarEast(),
                new GetOSMiddleEast(),
                new GetOSDecimalCharacter(),
                new GetOMAXLanguage(),
                new GetPathToWindowsDirectory(),
                new GetPathToProgramFilesDirectory(),
                new GetWindowsTemporaryDirectory(),
                new GetOMAXPublicDirectory()
            };

            return methods;
        }

        private static bool IsMadeByApple()
        {
            bool isMadeByApple = false;
            string keyName = "Manufacturer";

            string manufacturerName = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
            if (manufacturerName.ToUpper().Contains("APPLE"))
            {
                isMadeByApple = true;
            }

            return isMadeByApple;
        }

        private static bool IsServerVersion()
        {
            bool server = false;
            string keyName = "ProductType";
            string productTypeStringValue = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
            if (int.TryParse(productTypeStringValue, out int productTypeIntValue))
            {
                if (productTypeIntValue != 1)
                {
                    server = true;
                }
            }

            return server;
        }

        private static int MAKELANGID(int primary, int sub)
        {
            return (((ushort)sub) << 10) | ((ushort)primary);
        }

        private static int PRIMARYLANGID(int lcid)
        {
            return ((ushort)lcid) & 0x3ff;
        }

        private static int SUBLANGID(int lcid)
        {
            return ((ushort)lcid) >> 10;
        }







        private class GetHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "Operating System Details", InfoType.HEADER);
            }
        }



        private class IsUACEnabled : IGetInfo
        {
            public string OpName { get { return "UAC Enabled"; } }

            public Result GetInfo()
            {
                string returnValue = "UAC Enabled : ";
                string regKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System";
                string regValueKey = @"EnableLUA";
                int regValue = 0;

                using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                using (RegistryKey key = hklm.OpenSubKey(regKey))
                {
                    string str = key.GetValue(regValueKey).ToString();
                    regValue = int.Parse(str);
                }


                returnValue = (regValue != 0) ? "Enabled" : "NOT Enabled";
                return new Result(OpName, returnValue);
            }
        }



        private class GetOSBuildNumber : IGetInfo
        {
            public string OpName { get { return "OS Build Number"; } }
            public Result GetInfo()
            {
                string keyName = "BuildNumber";
                string buildNumber = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
                return new Result(OpName, buildNumber);
            }
        }



        private class GetOSMajor : IGetInfo
        {
            public string OpName { get { return "OS Major"; } }
            public Result GetInfo()
            {
                string osMajor = string.Empty;
                string keyName = "Version";
                string version = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
                string[] seps = version.Split('.');
                if (seps.Length >= 1)
                {
                    osMajor = seps[0];
                }

                return new Result(OpName, osMajor);
            }
        }


        private class GetOSMinor : IGetInfo
        {
            public string OpName { get { return "OS Minor"; } }
            public Result GetInfo()
            {
                string osMinor = "0";
                string keyName = "Version";
                string version = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
                string[] seps = version.Split('.');
                if (seps.Length >= 2)
                {
                    osMinor = seps[1];
                }

                return new Result(OpName, osMinor);
            }
        }


        private class GetOSName : IGetInfo
        {
            public string OpName { get { return "OS Name"; } }
            public Result GetInfo()
            {
                string keyName = "Caption";
                string osName = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
                return new Result(OpName, osName);
            }
        }


        private class GetOSServicePackMajorVersion : IGetInfo
        {
            public string OpName { get { return "OS Service Pack Major Version"; } }
            public Result GetInfo()
            {
                string keyName = "ServicePackMajorVersion";
                string osServicePackMajorVersion = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
                return new Result(OpName, osServicePackMajorVersion);
            }
        }


        private class GetOSServicePackMinorVersion : IGetInfo
        {
            public string OpName { get { return "OS Service Pack Minor Version"; } }
            public Result GetInfo()
            {
                string keyName = "ServicePackMinorVersion";
                string osServicePackMinorVersion = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
                return new Result(OpName, osServicePackMinorVersion);
            }
        }



        private class GetCompatabilityMode : IGetInfo
        {
            public string OpName { get { return "Compatability Mode"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be impelemented. This .exe is diff from WinMAKE.exe for determining compatability mode.");
            }
        }



        private class GetIsRunningUnderBootcamp : IGetInfo
        {
            public string OpName { get { return "Running under Bootcamp"; } }
            public Result GetInfo()
            {
                string runningUnderBootcamp = "False";
                if (IsMadeByApple())
                {
                    runningUnderBootcamp += "True";
                }


                return new Result(OpName, runningUnderBootcamp);
            }
        }



        private class GetIsRunningUnderVirtualMachine : IGetInfo
        {
            public string OpName { get { return "Running under VM"; } }
            public Result GetInfo()
            {
                string keyName = "HypervisorPresent";
                string vmPresent = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_ComputerSystem.ToString(), keyName);
                return new Result(OpName, vmPresent);
            }
        }



        private class GetOSLanguageName : IGetInfo
        {
            public string OpName { get { return "OS Language Name"; } }
            public Result GetInfo()
            {
                CultureInfo ci = CultureInfo.InstalledUICulture;
                return new Result(OpName, ci.EnglishName);
            }
        }



        private class GetOSLanguageLCID : IGetInfo
        {
            public string OpName { get { return "OS Language LCID"; } }
            public Result GetInfo()
            {
                string keyName = "OSLanguage";
                string languageLCID = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
                return new Result(OpName, languageLCID);
            }
        }



        private class GetOSPrimaryLanguageID : IGetInfo
        {
            public string OpName { get { return "OS Primary Language ID"; } }
            public Result GetInfo()
            {
                CultureInfo currentCulture = CultureInfo.CurrentCulture;
                int pid = PRIMARYLANGID(currentCulture.LCID);
                return new Result(OpName, pid.ToString());
            }
        }


        private class GetOSSubLanguageID : IGetInfo
        {
            public string OpName { get { return "OS Sub Language IDn"; } }
            public Result GetInfo()
            {
                CultureInfo currentCulture = CultureInfo.CurrentCulture;
                int sid = SUBLANGID(currentCulture.LCID);
                return new Result(OpName, sid.ToString());
            }
        }


        private class GetOSFarEast : IGetInfo
        {
            public string OpName { get { return "OS FarEast"; } }
            public Result GetInfo()
            {
                //string farEast = @"FarEast is true or nonzero if User32.dll supports DBCS; false or zero otherwise." +
                //             @" In other words, FarEast is true or nonzero if the double-byte character - set(DBCS) version of User.exe is installed; false or zero otherwise.";
                string propertyName = "DbcsEnabled";
                return new Result(OpName, Util.SystemInformation.GetSystemInformationPropertyValue(propertyName) + " result does NOT matche Delphi fn SysLocale.FarEast");
            }
        }


        private class GetOSMiddleEast : IGetInfo
        {
            public string OpName { get { return "OS MiddleEast"; } }
            public Result GetInfo()
            {
                // string middleEast = @"MiddleEast is true if the system is enabled for Hebrew and Arabic languages; false otherwise.";
                string propertyName = "MidEastEnabled";
                return new Result(OpName, Util.SystemInformation.GetSystemInformationPropertyValue(propertyName) + " result does NOT matche Delphi fn SysLocale.FarEast");
            }
        }


        private class GetOSDecimalCharacter : IGetInfo
        {
            public string OpName { get { return "OS Decimal Character"; } }
            public Result GetInfo()
            {
                decimal value = 16325.62m;
                string specifier;
                CultureInfo culture = CultureInfo.CurrentCulture;
                char decimalChar = '.';

                // Use standard numeric format specifiers.
                specifier = "G";
                string str = value.ToString(specifier, culture);
                foreach (char c in str)
                {
                    if (!Char.IsDigit(c))
                        decimalChar = c;
                }

                return new Result(OpName, decimalChar.ToString());
            }
        }


        private class GetOMAXLanguage : IGetInfo
        {
            public string OpName { get { return "OS OMAX Language"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (MAKE JSON OBJECT)  Multilizer Language Related.");
            }
        }


        private class GetPathToWindowsDirectory : IGetInfo
        {
            public string OpName { get { return "Windows Directory"; } }
            public Result GetInfo()
            {
                string keyName = "WindowsDirectory";
                string windowsDirectory = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_OperatingSystem.ToString(), keyName);
                return new Result(OpName, windowsDirectory);
            }
        }


        private class GetPathToProgramFilesDirectory : IGetInfo
        {
            public string OpName { get { return "OS Program Files Directory"; } }
            public Result GetInfo()
            {
                string programFilesDirectoryPath = Util.FilesAndFolders.GetOMAXLayoutAndMakeFolder();
                char c = '\\';
                int startIndex = Path.GetPathRoot(programFilesDirectoryPath).Length;
                int endIndex = programFilesDirectoryPath.IndexOf(c, startIndex);
                int length = endIndex;
                programFilesDirectoryPath = programFilesDirectoryPath.Substring(0, length);
                return new Result(OpName, programFilesDirectoryPath);
            }
        }



        private class GetWindowsTemporaryDirectory : IGetInfo
        {
            public string OpName { get { return "OS Temporary Path"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Path.GetTempPath());
            }
        }



        private class GetOMAXPublicDirectory : IGetInfo
        {
            public string OpName { get { return "OMAX Shared Path"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.FilesAndFolders.GetOMAXSharedFileRootFolder());
            }
        }





    }
}
