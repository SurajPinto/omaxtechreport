﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace TechReport
{
    class InfoProcesses : IGetInfoCollection
    {
        public List<IGetInfo> GetMethodList()
        {
            List<IGetInfo> methods = new List<IGetInfo>()
            {
                new GetInfoGetEmptyLines(2),
                new GetProcessRunningListHeader(),
                new GetProcessesRunningList(),
                new GetInfoGetEmptyLines(4),
                new GetProcessPrevelanceHeader(),
                new GetProcessPrevelanceList(),
                new GetInfoGetEmptyLines(4),
                new GetApplicationsDirectoryHeader(),
                new GetAllFilesAndFoldersInApplicationsDirectory(),
                new GetInfoGetEmptyLines(4),
                new GetHelpFolderHeader(),
                new GetAllFilesAndFoldersInHelpDirectory(),
                new GetInfoGetEmptyLines(4),
                new GetDOSSoftwareHeader(),
                new GetDosSoftwareInstalled(),
                new GetInfoEmptyLine(),
                new GetExistsAutoExecBatHeader(),
                new GetExistsAutoExecBat(),
                new GetInfoEmptyLine(),
                new GetExistsConfigSysHeader(),
                new GetExistsConfigSys()
            };

            return methods;
        }

        private static void GetFilesAndFolderDetails(string directoryName, ref List<string> list)
        {
            int count = 0;

            string[] fileList = Directory.GetFiles(directoryName);
            foreach (string filename in fileList)
            {
                //FileVersionInfo verInfo = FileVersionInfo.GetVersionInfo(filename);
                DateTime lastModifiedTime = File.GetLastWriteTime(filename);
                list.Add($"{++count,3}. {lastModifiedTime:yyyy-mm-dd HH-mm}     {filename}");
            }

            count = 0;

            // blank line to separate files and folder details
            list.Add(string.Empty);

            string[] directoryList = Directory.GetDirectories(directoryName);
            foreach (string directoryname in directoryList)
            {
                list.Add($"{++count,3}. {"Folder"}     {directoryname}");
            }
        }





        private class GetProcessRunningListHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "Processes", InfoType.HEADER);
            }
        }

        private class GetProcessesRunningList : IGetInfo
        {
            public string OpName { get { return "Process Executing"; } }
            public Result GetInfo()
            {
                int count = 0;
                List<string> result = new List<string>();
                Process[] processList = Process.GetProcesses();
                foreach (Process p in processList)
                {
                    ++count;
                    result.Add($"{count,4} {p.ProcessName}");
                }

                return new Result(OpName, result);
            }
        }



        private class GetProcessPrevelanceHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "Process Prevelance", InfoType.HEADER);
            }
        }



        private class GetProcessPrevelanceList : IGetInfo
        {
            public string OpName { get { return "Process Prevelance"; } }
            public Result GetInfo()
            {
                int count = 0;
                List<string> result = new List<string>();
                Process[] processList = Process.GetProcesses();

                foreach (Process p in processList)
                {
                    string processFilename = Path.GetFileName(p.ProcessName);
                    string processFilenameWithoutExtension = Path.GetFileNameWithoutExtension(p.ProcessName);
                    if (string.Equals(processFilename, processFilenameWithoutExtension, StringComparison.InvariantCultureIgnoreCase))
                    {
                        processFilename += ".exe";
                    }
                    count = UtilProcessMonitor.HowNormalIsProcess(processFilename);
                    string str = $"{count,3}      {processFilename}";
                    if (count == 0)
                    {
                        str += "   <<<<<--- An unusual process, perhaps worth investigating";
                    }

                    result.Add(str);
                }

                return new Result(OpName, result);
            }
        }



        private class GetApplicationsDirectoryHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "Applications/Install Folder Details", InfoType.HEADER);
            }
        }

        private class GetAllFilesAndFoldersInApplicationsDirectory : IGetInfo
        {
            public string OpName { get { return "All files and folders installed into Applications Directory"; } }

            public Result GetInfo()
            {
                string installedDirectory = Util.FilesAndFolders.GetOMAXLayoutAndMakeFolder();
                List<string> result = new List<string>();
                GetFilesAndFolderDetails(installedDirectory, ref result);
                return new Result(OpName, result);
            }
        }




        private class GetHelpFolderHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "Help Folder Details", InfoType.HEADER);
            }
        }

        private class GetAllFilesAndFoldersInHelpDirectory : IGetInfo
        {
            public string OpName { get { return "All files and folders installed into Help Directory"; } }

            public Result GetInfo()
            {
                bool helpFileDirectoryDetermined = false;
                string helpDirectoryName = string.Empty;
                List<string> result = new List<string>();

                List<string> dirList = new List<string>();
                string applicationLaunchDirectory = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
                string regularHelpDirectoryName = Util.FilesAndFolders.OMAXReferenceFolderName;
                string alternateHelpDirectoryName = "OMAX_Interactive_Reference";
                string developmentFolderName = @"D:\Development\Documents\Help_Screens\OMAX_Interactive_Reference";

                // get path of official help file directory
                dirList.Add(Path.Combine(Util.FilesAndFolders.GetOMAXLayoutAndMakeFolder(), Util.FilesAndFolders.OMAXReferenceFolderName));

                // look in the directory where the application was launched
                dirList.Add(Path.Combine(applicationLaunchDirectory, regularHelpDirectoryName));

                // look in directory where application was launched with alternate help directory name
                dirList.Add(Path.Combine(applicationLaunchDirectory, alternateHelpDirectoryName));

                // look in development folder
                dirList.Add(Path.Combine(developmentFolderName, regularHelpDirectoryName));
                dirList.Add(Path.Combine(developmentFolderName, alternateHelpDirectoryName));

                foreach (string directoryName in dirList)
                {
                    if (Directory.Exists(directoryName))
                    {
                        helpDirectoryName = directoryName;
                        helpFileDirectoryDetermined = true;
                        break;
                    }
                }

                if (helpFileDirectoryDetermined == true)
                {
                    result.Add("This function requires revisiting. To be implemented. (MAKE JSON OBJECT). Multilizer Language Related.");
                    result.Add(string.Empty);
                    GetFilesAndFolderDetails(helpDirectoryName, ref result);
                }
                else
                {
                    result.Add("HELP Directory unable to be determined.  To be implemented. (MAKE JSON OBJECT). Multilizer Language Related.");
                }

                return new Result(OpName, result);
            }
        }


        private class GetDOSSoftwareHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "OMAX Dos Software", InfoType.HEADER);
            }
        }

        private class GetDosSoftwareInstalled : IGetInfo
        {
            public string OpName { get { return "Dos Software on this computer"; } }

            public Result GetInfo()
            {
                List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
                string omaxDosExeFilename = @"C:\OMAX\OMAX.EXE";

                if (File.Exists(omaxDosExeFilename))
                {
                    result.Add(new KeyValuePair<string, string>("OMAX DOS Software installed in this computer", "Yes"));
                    DateTime lastModifiedTime = File.GetLastWriteTime(omaxDosExeFilename);
                    result.Add(new KeyValuePair<string, string>("Last Modified Time", $"{lastModifiedTime:yyyy-mm-dd hh-mm}"));

                    string omaxTdcFilename = @"C:\OMAX\TDC.TDC";
                    if (File.Exists(omaxTdcFilename))
                    {
                        string[] fileData = File.ReadAllLines(omaxTdcFilename);
                        foreach (string fileDataLine in fileData)
                        {
                            result.Add(new KeyValuePair<string, string>(fileDataLine, string.Empty));
                        }
                    }
                    else
                    {
                        result.Add(new KeyValuePair<string, string>("No time code available!", "Perhaps DOS software is not fully installed"));
                    }
                }
                else
                {
                    result.Add(new KeyValuePair<string, string>("OMAX DOS Software installed in this computer", "NO"));
                }


                return new Result(OpName, result);
            }
        }





        private class GetExistsAutoExecBatHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }

            public Result GetInfo()
            {
                return new Result(OpName, "AUTOEXEC.BAT", InfoType.HEADER);
            }
        }


        private class GetExistsAutoExecBat : IGetInfo
        {
            public string OpName { get { return @"C:\AUTOEXEC.BAT exists"; } }

            public Result GetInfo()
            {
                string keyStr = @"C:\AUTOEXEC.BAT file exists on this computer";
                string autoexecBatFilename = @"C:\AUTOEXEC.BAT";
                string valueStr = "NO";
                List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
                if (File.Exists(autoexecBatFilename))
                {
                    valueStr = "YES";
                }

                result.Add(new KeyValuePair<string, string>(keyStr, valueStr));
                return new Result(OpName, result);
            }
        }





        private class GetExistsConfigSysHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }

            public Result GetInfo()
            {
                return new Result(OpName, "CONFIG.SYS", InfoType.HEADER);
            }
        }

        private class GetExistsConfigSys : IGetInfo
        {
            public string OpName { get { return @"C:\CONFIG.SYS exists"; } }

            public Result GetInfo()
            {
                string keyStr = @"C:\CONFIG.SYS file exists on this computer";
                string configSysFilename = @"C:\CONFIG.SYS";
                string valueStr = "NO";
                List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
                if (File.Exists(configSysFilename))
                {
                    valueStr = "YES";
                }

                result.Add(new KeyValuePair<string, string>(keyStr, valueStr));
                return new Result(OpName, result);
            }
        }





    }
}
