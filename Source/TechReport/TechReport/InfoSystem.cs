﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace TechReport
{
    class InfoSystem : IGetInfoCollection
    {
        public List<IGetInfo> GetMethodList()
        {
            List<IGetInfo> methods = new List<IGetInfo>()
            {
                new GetHeader(),
                new GetProcessorCount(),
                new GetLogicalProcessorCount(),
                new GetProcessorSpeed(),
                new GetCurrentlyConnectedToWWW(),
                new GetMinutesSinceLastReboot(),
                new GetDaysSinceLastReboot(),
                new GetWinUpdatesRebootAllowed(),
                new GetMemoryLoad(),
                new GetTotalPhysicalMemory(),
                new GetAvailablePhysicalMemory(),
                new GetTotalVirtualMemory(),
                new GetAvailableVirtualMemory(),
                new GetMemoryTakenByWinMake(),
                new GetNumberOfPrinters(),
                new GetMousePresent(),
                new GetMouseWheelPresent(),
                new GetMouseWheelScrollLines(),
                new GetJoystickPresent(),
                new GetSoundCardWaveformInputs(),
                new GetSoundCardWaveformOutputs(),
                new GetInfoEmptyLine(),
                new GetScreenResolution(),
                new GetColorBitDepth(),
                new GetNumberOfColors(),
                new GetPixelsPerInch(),
                new GetTrueDPIRatio(),
                new GetWindowsFontSizeFromRegistry(),
                new GetIsWindowsAeroEnabled(),
                new GetMonitorsAttached(),
                new GetApplicationFormScreenWidth(),
                new GetApplicationFormScreenHeight(),
                new GetMonitorsCount(),
                new GetMonitorDetails(),
                new GetCursorCount(),
                new GetGraphicCardDetails(),
                new HardDiskDetails()
            };


            return methods;
        }









        private class GetHeader : IGetInfo
        {
            public string OpName { get { return "HEADER"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "System Details", InfoType.HEADER);
            }
        }



        private class GetProcessorCount : IGetInfo
        {
            public string OpName { get { return "NumberOfCores"; } }
            public Result GetInfo()
            {
                string keyName = "NumberOfCores";
                string numOfCores = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_Processor.ToString(), keyName);
                return new Result(OpName, numOfCores);
            }
        }

        private class GetLogicalProcessorCount : IGetInfo
        {
            public string OpName { get { return "Logical NumberOfCores"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Environment.ProcessorCount.ToString());

            }
        }



        private class GetProcessorSpeed : IGetInfo
        {
            public string OpName { get { return "Approximate Processor Speed"; } }
            public Result GetInfo()
            {
                string keyName = "CurrentClockSpeed";
                string currentClockSpeed = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_Processor.ToString(), keyName);
                return new Result(OpName, currentClockSpeed);
            }
        }


        private class GetCurrentlyConnectedToWWW : IGetInfo
        {
            public string OpName { get { return "Currently connected to www"; } }
            public Result GetInfo()
            {
                bool networkConnectivity = false;
                string macAddress = string.Empty;

                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
                    foreach (NetworkInterface i in interfaces)
                    {
                        if (i.NetworkInterfaceType == NetworkInterfaceType.Tunnel || i.NetworkInterfaceType == NetworkInterfaceType.Loopback)
                            continue;
                        if (i.Description.ToLower().Contains("virtual"))
                            continue;

                        if (i.OperationalStatus == OperationalStatus.Up)
                        {
                            IPInterfaceStatistics ipStats = i.GetIPStatistics();
                            IPv4InterfaceStatistics ipv4Stats = i.GetIPv4Statistics();
                            if ((ipStats.BytesReceived > 0 && ipStats.BytesSent > 0) || (ipv4Stats.BytesReceived > 0) && (ipv4Stats.BytesSent > 0))
                                networkConnectivity = true;
                        }
                    }
                }

                return new Result(OpName, networkConnectivity.ToString());
            }
        }


        private class GetMinutesSinceLastReboot : IGetInfo
        {
            public string OpName { get { return "Minutes Since Last Reboot"; } }
            public Result GetInfo()
            {
                string minutesSinceReboot = string.Empty;
                TimeSpan timespanSinceReboot = TimeSpan.MinValue;
                using (var uptime = new PerformanceCounter("System", "System Up Time"))
                {
                    uptime.NextValue();       //Call this an extra time before reading its value
                    timespanSinceReboot = TimeSpan.FromSeconds(uptime.NextValue());
                    minutesSinceReboot = timespanSinceReboot.TotalMinutes.ToString();
                }

                return new Result(OpName, minutesSinceReboot);
            }
        }


        private class GetHoursSinceLastReboot : IGetInfo
        {
            public string OpName { get { return "Hours Since Last Reboot"; } }
            public Result GetInfo()
            {
                string hoursSinceReboot = string.Empty;
                TimeSpan timespanSinceReboot = TimeSpan.MinValue;
                using (var uptime = new PerformanceCounter("System", "System Up Time"))
                {
                    uptime.NextValue();       //Call this an extra time before reading its value
                    timespanSinceReboot = TimeSpan.FromSeconds(uptime.NextValue());
                    hoursSinceReboot = timespanSinceReboot.TotalHours.ToString();
                }

                return new Result(OpName, hoursSinceReboot);
            }
        }


        private class GetDaysSinceLastReboot : IGetInfo
        {
            public string OpName { get { return "Days Since Last Reboot"; } }
            public Result GetInfo()
            {
                string daysSinceReboot = string.Empty;
                TimeSpan timespanSinceReboot = TimeSpan.MinValue;
                using (var uptime = new PerformanceCounter("System", "System Up Time"))
                {
                    uptime.NextValue();       //Call this an extra time before reading its value
                    timespanSinceReboot = TimeSpan.FromSeconds(uptime.NextValue());
                    daysSinceReboot = timespanSinceReboot.TotalDays.ToString();
                }

                return new Result(OpName, daysSinceReboot);
            }
        }


        private class GetWinUpdatesRebootAllowed : IGetInfo
        {
            public string OpName { get { return "Win Updates Reboot Allowed With Logged On Users"; } }
            public Result GetInfo()
            {
                string returnValue = string.Empty;
                string regKeyName = @"SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU";
                string regKeyValueName = "NoAutoRebootWithLoggedOnUsers";
                string rebootAllowed = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.LocalMachine, regKeyName, regKeyValueName, string.Empty);
                if (string.Equals(rebootAllowed, "0", StringComparison.InvariantCultureIgnoreCase))
                    returnValue = "False";
                else
                    returnValue = "True";

                return new Result(OpName, returnValue);
            }
        }


        private class GetMemoryLoad : IGetInfo
        {
            public string OpName { get { return "Memory Load"; } }
            public Result GetInfo()
            {
                string keyName = "TotalPhysicalMemory";
                double totalMem = Convert.ToDouble(UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_ComputerSystem.ToString(), keyName));
                PerformanceCounter performance = new PerformanceCounter("Memory", "Available Bytes");
                double availableMem = performance.NextValue();

                Int64 load = Convert.ToInt64(totalMem - availableMem) * 100 / Convert.ToInt64(totalMem);
                return new Result(OpName, load.ToString());
            }
        }


        private class GetTotalPhysicalMemory : IGetInfo
        {
            public string OpName { get { return "Total Physical Memory"; } }
            public Result GetInfo()
            {
                string keyName = "TotalPhysicalMemory";
                string memSize = UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_ComputerSystem.ToString(), keyName);
                string formattedMemSize = Util.Formatting.BytesValueStringFormattedOutputString(Convert.ToDouble(memSize));
                return new Result(OpName, formattedMemSize);
            }
        }


        private class GetAvailablePhysicalMemory : IGetInfo
        {
            public string OpName { get { return "Available Physical Memory"; } }
            public Result GetInfo()
            {
                PerformanceCounter performance = new PerformanceCounter("Memory", "Available Bytes");
                double memSize = performance.NextValue();
                string formattedMemSize = Util.Formatting.BytesValueStringFormattedOutputString(memSize);
                return new Result(OpName, formattedMemSize);
            }
        }


        private class GetTotalVirtualMemory : IGetInfo
        {
            public string OpName { get { return "Total Virtual Memory"; } }
            public Result GetInfo()
            {
                PerformanceCounter performance = new PerformanceCounter("Memory", "Commit Limit");
                double memSize = performance.NextValue();
                string formattedMemSize = Util.Formatting.BytesValueStringFormattedOutputString(memSize);
                return new Result(OpName, formattedMemSize);
            }
        }


        private class GetAvailableVirtualMemory : IGetInfo
        {
            public string OpName { get { return "Available Virtual Memory"; } }
            public Result GetInfo()
            {
                PerformanceCounter used = new PerformanceCounter("Memory", "Committed Bytes");
                PerformanceCounter total = new PerformanceCounter("Memory", "Commit Limit");
                double usedVirtualMemory = Convert.ToDouble(used.NextValue());
                double totalVirtualMemory = Convert.ToDouble(total.NextValue());
                double availableVirtualMem = totalVirtualMemory - usedVirtualMemory;
                string formattedMemSize = Util.Formatting.BytesValueStringFormattedOutputString(availableVirtualMem);
                return new Result(OpName, formattedMemSize);
            }
        }


        private class GetMemoryTakenByWinMake : IGetInfo
        {
            private string application = "WinMAKE.exe";
            private string processName = "WinMAKE";
            private int count = 0;
            public string OpName { get { return $"{application} total instances running {count} is using : "; } }
            public Result GetInfo()
            {
                double totalMemory = 0;
                count = 0;

                Process[] processList = Process.GetProcessesByName(processName);
                foreach (Process p in processList)
                {
                    count++;
                    totalMemory += Convert.ToDouble(p.PrivateMemorySize64);
                }

                return new Result(OpName, Util.Formatting.BytesValueStringFormattedOutputString(totalMemory));
            }
        }

        private class GetNumberOfPrinters : IGetInfo
        {
            public string OpName { get { return "Number of Printers"; } }
            public Result GetInfo()
            {
                //return new Result(OpName, UtilSysMgmt.GetResultsFor(UtilSysMgmt.SYS_MANAGEMENT.Win32_Printer.ToString()).Count.ToString());
                // foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                return new Result(OpName, PrinterSettings.InstalledPrinters.Count.ToString());
            }
        }

        private class GetMousePresent : IGetInfo
        {
            public string OpName { get { return "Mouse Present"; } }
            public Result GetInfo()
            {
                string mousePresentStr = "False";
                string propertyName = "MousePresent";
                mousePresentStr = Util.SystemInformation.GetSystemInformationPropertyValue(propertyName);
                return new Result(OpName, mousePresentStr);
            }
        }


        private class GetMouseWheelPresent : IGetInfo
        {
            public string OpName { get { return "Mouse Wheel"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.SystemInformation.GetSystemInformationPropertyValue("MouseWheelPresent"));
            }
        }


        private class GetMouseWheelScrollLines : IGetInfo
        {
            public string OpName { get { return "Mouse Wheel Scroll Lines"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.SystemInformation.GetSystemInformationPropertyValue("MouseWheelScrollLines"));
            }
        }


        private class GetJoystickPresent : IGetInfo
        {

            [DllImport("winmm.dll")]
            static extern uint joyGetNumDevs();

            [DllImport("winmm.dll")]
            static extern uint joyGetDevCaps(uint uJoyID, ref JOYCAPS jwoc, uint cbwoc);

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
            private struct JOYCAPS
            {
                public ushort wMid;
                public ushort wPid;
                [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
                public string szPname;
                public uint wXmin;
                public uint wXmax;
                public uint wYmin;
                public uint wYmax;
                public uint wZmin;
                public uint wZmax;
                public uint wNumButtons;
                public uint wPeriodMin;
                public uint wPeriodMax;
                public uint wRmin;
                public uint wRmax;
                public uint wUmin;
                public uint wUmax;
                public uint wVmin;
                public uint wVmax;
                public uint wCaps;
                public uint wMaxAxes;
                public uint wNumAxes;
                public uint wMaxButtons;
                [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
                public string szRegKey;
                [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
                public string szOEMVxD;
            }

            public string OpName { get { return "Joystick Present"; } }
            public Result GetInfo()
            {
                uint numJoysticks = 0;
                JOYCAPS caps = new JOYCAPS();
                List<string> joyStickNamesList = new List<string>();

                // get number of joy stick devices supported by driver
                numJoysticks = joyGetNumDevs();

                string[] joyStickNames = new string[numJoysticks];
                int joyStickManufactuereId = 0;
                for (uint i = 0; i < numJoysticks; i++)
                {
                    // get joy stick device capabilities information
                    joyGetDevCaps(i, ref caps, (uint)Marshal.SizeOf(caps));


                    // get joy stick manufacturer id and joy stick product name
                    joyStickManufactuereId = caps.wMid;
                    joyStickNames[i] = caps.szPname.Trim();


                    // if joy stick manufacturer ID is not 0 and joy stick product name is not null or empty 
                    if ((joyStickManufactuereId != 0) && (!string.IsNullOrEmpty(joyStickNames[i])))
                    {
                        // add joy stick name to result list
                        joyStickNamesList.Add(joyStickNames[i]);
                    }
                }

                return new Result(OpName, joyStickNamesList.Count.ToString());
            }
        }



        private class GetSoundCardWaveformInputs : IGetInfo
        {
            [StructLayout(LayoutKind.Sequential, Pack = 4)]
            private struct WaveInCaps
            {
                public short wMid;
                public short wPid;
                public int vDriverVersion;
                [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
                public char[] szPname;
                public uint dwFormats;
                public short wChannels;
                public short wReserved1;
            }

            [DllImport("winmm.dll")]
            private static extern int waveInGetNumDevs();
            [DllImport("winmm.dll", EntryPoint = "waveInGetDevCaps")]
            private static extern int waveInGetDevCapsA(int uDeviceID, ref WaveInCaps lpCaps, int uSize);



            public string OpName { get { return "Sound Card Waveform Inputs"; } }
            public Result GetInfo()
            {
                int waveInDevicesCount = waveInGetNumDevs();
                string[] result = new string[waveInDevicesCount];
                if (waveInDevicesCount > 0)
                {
                    for (int uDeviceID = 0; uDeviceID < waveInDevicesCount; uDeviceID++)
                    {
                        WaveInCaps waveInCaps = new WaveInCaps();
                        waveInGetDevCapsA(uDeviceID, ref waveInCaps, Marshal.SizeOf(typeof(WaveInCaps)));
                        string deviceName = new string(waveInCaps.szPname).Remove(new string(waveInCaps.szPname).IndexOf('\0')).Trim();
                        result[uDeviceID] = deviceName;
                    }
                }

                return new Result(OpName, waveInDevicesCount.ToString());
            }
        }



        private class GetSoundCardWaveformOutputs : IGetInfo
        {
            [DllImport("winmm.dll", SetLastError = true)]
            private static extern uint waveOutGetNumDevs();

            [DllImport("winmm.dll", SetLastError = true, CharSet = CharSet.Auto)]
            private static extern uint waveOutGetDevCaps(uint hwo, ref WAVEOUTCAPS pwoc, uint cbwoc);

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
            private struct WAVEOUTCAPS
            {
                public ushort wMid;
                public ushort wPid;
                public uint vDriverVersion;
                [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
                public string szPname;
                public uint dwFormats;
                public ushort wChannels;
                public ushort wReserved1;
                public uint dwSupport;
            }


            public string OpName { get { return "Sound Card Waveform Outputs"; } }
            public Result GetInfo()
            {
                uint waveOutDevicesCount = waveOutGetNumDevs();
                string[] result = new string[waveOutDevicesCount];
                WAVEOUTCAPS caps = new WAVEOUTCAPS();

                for (uint i = 0; i < waveOutDevicesCount; i++)
                {
                    waveOutGetDevCaps(i, ref caps, (uint)Marshal.SizeOf(caps));
                    result[i] = caps.szPname;
                }

                return new Result(OpName, waveOutDevicesCount.ToString());
            }
        }



        private class GetScreenResolution : IGetInfo
        {
            public string OpName { get { return "Screen Resolution"; } }
            public Result GetInfo()
            {
                string propertyName = "VirtualScreen";
                Rectangle virtualScreen = new Rectangle(0, 0, 0, 0);
                Type t = typeof(System.Windows.Forms.SystemInformation);
                PropertyInfo[] pi = t.GetProperties();
                for (int i = 0; i < pi.Length; i++)
                {
                    if (string.Equals(pi[i].Name, propertyName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        object propObjValue = pi[i].GetValue(null, null);
                        if (propObjValue is Rectangle)
                            virtualScreen = (Rectangle)propObjValue;
                        break;
                    }
                }

                string screenResolution = $"{virtualScreen.Width} x {virtualScreen.Height}";
                return new Result(OpName, screenResolution);
            }
        }


        private class GetColorBitDepth : IGetInfo
        {
            public string OpName { get { return "Color Bit Depth"; } }
            public Result GetInfo()
            {
                return new Result(OpName, UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_VideoController.ToString(), "CurrentBitsPerPixel"));
            }
        }



        private class GetNumberOfColors : IGetInfo
        {
            public string OpName { get { return "Number of Colors"; } }
            public Result GetInfo()
            {
                return new Result(OpName, UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_VideoController.ToString(), "CurrentNumberOfColors"));
            }
        }



        private class GetPixelsPerInch : IGetInfo
        {
            public string OpName { get { return "Pixels Per Inch"; } }
            public Result GetInfo()
            {
                return new Result(OpName, UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_DesktopMonitor.ToString(), "PixelsPerXLogicalInch"));
            }
        }



        private class GetTrueDPIRatio : IGetInfo
        {
            public string OpName { get { return "Get TrueDPI Ratio"; } }
            public Result GetInfo()
            {
                string iniFilename = Path.Combine(Util.FilesAndFolders.GetOMAXSharedFileRootFolder(), "Display.ini");
                string trueDPIRatio = Util.Ini.ReadValue("HighDPI", "DPIRationB", iniFilename, "1");
                return new Result(OpName, trueDPIRatio);
            }
        }



        private class GetWindowsFontSizeFromRegistry : IGetInfo
        {
            public string OpName { get { return "Windows Font Size from Registry"; } }
            public Result GetInfo()
            {
                string regKey = @"Control Panel\Desktop\WindowMetrics";
                string regValueKey = "AppliedDPI";
                string fontSize = Util.Registry.ReadRegistryKey64BitHive(RegistryHive.CurrentUser, regKey, regValueKey, string.Empty);
                return new Result(OpName, fontSize);
            }
        }



        private class GetIsWindowsAeroEnabled : IGetInfo
        {
            [DllImport("dwmapi.dll", EntryPoint = "DwmIsCompositionEnabled")]
            private static extern int DwmIsCompositionEnabled(out bool enabled);


            public string OpName { get { return "Is Windows Aero Enabled"; } }
            public Result GetInfo()
            {
                bool aeroEnabled = false;

                // This function DwmIsCompositionEnabled is only supported in versions of Windows starting with Vista, 
                // so check the OS version to be Vista or later i.e. OSVersion.Version.Major >= 5 
                if (Environment.OSVersion.Version.Major > 5)
                {
                    DwmIsCompositionEnabled(out aeroEnabled);
                }


                return new Result(OpName, aeroEnabled.ToString());
            }
        }



        private class GetMonitorsAttached : IGetInfo
        {
            public string OpName { get { return "Monitors attached"; } }
            public Result GetInfo()
            {
                return new Result(OpName, Util.SystemInformation.GetSystemInformationPropertyValue("MonitorCount"));
            }
        }



        private class GetApplicationFormScreenWidth : IGetInfo
        {
            public string OpName { get { return "ApplicationFormScreenWidth"; } }
            public Result GetInfo()
            {
                return new Result(OpName, UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_VideoController.ToString(), "CurrentHorizontalResolution"));
            }
        }



        private class GetApplicationFormScreenHeight : IGetInfo
        {
            public string OpName { get { return "ApplicationFormScreenHeight"; } }
            public Result GetInfo()
            {
                return new Result(OpName, UtilSysMgmt.GetValueForKeyInOnlyResult(UtilSysMgmt.SYS_MANAGEMENT.Win32_VideoController.ToString(), "CurrentVerticalResolution"));
            }
        }



        private class GetMonitorsCount : IGetInfo
        {
            public string OpName { get { return "Monitors attached"; } }
            public Result GetInfo()
            {
                string monitorCount = "Is this different from already retrieved value 'Monitors attached'";
                return new Result(OpName, monitorCount);
            }
        }



        private class GetCursorCount : IGetInfo
        {
            public string OpName { get { return "Current Cursor Count"; } }
            public Result GetInfo()
            {
                return new Result(OpName, "To be implemented. (Delphi Environment value).");
            }
        }



        private class GetMonitorDetails : IGetInfo
        {
            public string OpName { get { return "MonitorDetails"; } }
            public Result GetInfo()
            {
                int count = 0;
                //StringBuilder strBuilder = new StringBuilder();
                List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();

                foreach (Screen screen in Screen.AllScreens)
                {
                    string prefix = $"Screen.Monitor[{count}]";
                    result.Add(new KeyValuePair<string, string>($"{prefix}.MonitorNum", count.ToString()));
                    result.Add(new KeyValuePair<string, string>($"{prefix}.Primary", screen.Primary.ToString()));
                    result.Add(new KeyValuePair<string, string>($"{prefix}.Width", screen.Bounds.Width.ToString()));
                    result.Add(new KeyValuePair<string, string>($"{prefix}.Height", screen.Bounds.Height.ToString()));
                    result.Add(new KeyValuePair<string, string>($"{prefix}.Top", screen.Bounds.Top.ToString()));
                    result.Add(new KeyValuePair<string, string>($"{prefix}.Left", screen.Bounds.Left.ToString()));
                    count++;
                }

                return new Result(OpName, result);
            }
        }



        private class GetGraphicCardDetails : IGetInfo
        {
            public string OpName { get { return "VideoDisplay Details"; } }
            public Result GetInfo()
            {
                string keyName = "Name";
                StringBuilder strBuilder = new StringBuilder();
                List<Dictionary<string, string>> result = UtilSysMgmt.GetAllResults(UtilSysMgmt.SYS_MANAGEMENT.Win32_VideoController.ToString());
                int count = 0;
                foreach (var videoCard in result)
                {
                    if (videoCard.ContainsKey(keyName))
                    {
                        strBuilder.Append($"Video display {count++} : {videoCard[keyName]}");
                    }
                }

                return new Result(OpName, strBuilder.ToString());
            }
        }



        public class HardDiskDetails : IGetInfo
        {
            public string OpName { get { return "Visible Disk Drives"; } }
            public Result GetInfo()
            {
                List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
                int count = 0;
                foreach (DriveInfo d in DriveInfo.GetDrives())
                {
                    try
                    {
                        string totalSpace = Util.Formatting.BytesValueStringFormattedOutputString(d.TotalFreeSpace);
                        string freeSpace = Util.Formatting.BytesValueStringFormattedOutputString(d.AvailableFreeSpace);
                        string value = $"{d.Name,5} {d.DriveType,10} { d.DriveFormat,5}  TotalSize: {totalSpace,-10}   AvailableFreeSpace: {freeSpace,-10}";
                        result.Add(new KeyValuePair<string, string>($"Drive {count++}", value));
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                return new Result(OpName, result);
            }
        }





    }
}
