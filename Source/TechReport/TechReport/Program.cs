﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using static System.Console;

namespace TechReport
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            // Set the Thread Prioty and Process Priority to the lowest, so that it does not take CPU cycles away from any other process
            // This is very important as report generation could be run while the machine is cutting a model
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.Idle;
            Thread.CurrentThread.Priority = ThreadPriority.Lowest;



            List<Exception> exceptionList = new List<Exception>();
            List<IGetInfoCollection> coll = new List<IGetInfoCollection>()
            {
                new InfoApplicationDetails(),
                new InfoSystem(),
                new InfoOS(),
                new InfoEnvironment(),
                new InfoProcesses(),
                new InfoIni()
            };





            // To Do : Document Start Formatting



            foreach (IGetInfoCollection collItem in coll)
            {
                // To Do : Paragraph Start Formatting



                int count = -1;
                foreach (IGetInfo infoItem in collItem.GetMethodList())
                {
                    try
                    {
                        count++;
                        Result result = infoItem.GetInfo();


                        // Each iteration of this loop provides one result
                        // formatting such as Center Align can be done at the paragraph level
                        // If Header of paragraph requires formatting, can be done inside the if statement


                        if (result.ResultType == InfoType.HEADER)
                        {
                            // Header
                            WriteLine($"{infoItem.GetInfo().GetResultStr().ToUpper()}");
                        }
                        else if (result.ResultType == InfoType.EMPTY_LINE)
                        {
                            // Blank Line (for space)
                            WriteLine(result.GetResultStr());
                        }
                        if (result.ResultType == InfoType.SINGLE_LINE)
                        {
                            // One line result
                            WriteLineToConsole(infoItem.OpName, result.GetResultStr(), count);
                        }
                        else if (result.ResultType == InfoType.MULTI_LINE)
                        {
                            // Multiple Lines of Result
                            List<string> multiLineResult = result.GetResultList();
                            foreach (string singleLineResult in multiLineResult)
                            {
                                WriteLine(singleLineResult);
                            }
                        }
                        else if (result.ResultType == InfoType.MULTI_LINE_KEY_VALUE)
                        {
                            // Multiple Lines of Result of type KeyValuePair
                            List<KeyValuePair<string, string>> multiLineResult = result.GetResultKeyValueList();
                            foreach (KeyValuePair<string, string> singleLineResult in multiLineResult)
                            {
                                WriteLineToConsole(singleLineResult.Key, singleLineResult.Value, count);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteLine($"{infoItem.OpName} : EXCEPTION    {ex.Message} {ex.GetType()}");
                        exceptionList.Add(ex);
                    }
                }

                // To Do : Paragraph End Formatting



                WriteLine("\n----------------------------------------------------\n");
            }

            // To Do : Document End Formatting






            WriteLine("\n\n");

            // Execution Report
            if (exceptionList.Count == 0)
            {
                WriteLine("Every method call executed successfully");
            }
            else
            {
                WriteLine($"{exceptionList.Count} methods did not complete, exception details below");
                int count = 0;
                foreach (Exception e in exceptionList)
                {
                    WriteLine($"{++count}:   message: {e.Message}        type: {e.GetType()}");
                    WriteLine(e.StackTrace);
                    WriteLine();
                }
            }

            WriteLine("Press a key to exit program");
            ReadKey();
        }

        private static void WriteLineToConsole(string opname, string value, int count)
        {
            const int LINE_LENGTH = 100;
            const int LINE_MID = 48;

            int length = opname.Length + value.Length + 5;
            if (length < 80 && opname.Length < LINE_MID && value.Length < LINE_MID)
            {
                StringBuilder displayLine = new StringBuilder(LINE_LENGTH);
                displayLine.Insert(0, " ", LINE_LENGTH);

                for (int i = 0; i < opname.Length; i++)
                {
                    displayLine[LINE_MID - opname.Length + i] = opname[i];
                }

                displayLine[LINE_MID + 1] = ':';
                for (int i = 0; i < value.Length; i++)
                {
                    displayLine[LINE_MID + 3 + i] = value[i];
                }

                WriteLine(displayLine.ToString());
            }
            else
            {
                WriteLine($"{opname} : {value}");
            }
        }
    }
}
