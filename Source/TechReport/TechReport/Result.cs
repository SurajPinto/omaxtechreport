﻿using System;
using System.Collections.Generic;

namespace TechReport
{
    class Result
    {
        private string _opName = string.Empty;
        private string _valueStr = string.Empty;
        private List<string> _valueStrList = null;
        private List<KeyValuePair<string, string>> _valueKeyValuePairList = null;
        public InfoType ResultType { get; private set; }

        public Result(string opName, string str)
        {
            _opName = opName;
            _valueStr = str;
            ResultType = InfoType.SINGLE_LINE;
        }

        public Result(string opName, List<string> list)
        {
            _opName = opName;
            _valueStrList = list;
            ResultType = InfoType.MULTI_LINE;
        }

        public Result(string opName, List<KeyValuePair<string, string>> list)
        {
            _opName = opName;
            _valueKeyValuePairList = list;
            ResultType = InfoType.MULTI_LINE_KEY_VALUE;
        }

        public Result(string opName, string str, InfoType resultType)
        {
            _opName = opName;
            _valueStr = str;
            ResultType = resultType;
        }

        public string GetResultStr()
        {
            if (ResultType == InfoType.MULTI_LINE || ResultType == InfoType.MULTI_LINE_KEY_VALUE)
            {
                throw new InvalidOperationException("Cannot call GetResultStr when result is InfoType.MULTI_LINE_KEY_VALUE OR InfoType.MULTI_LINE_KEY_VALUE.");
            }

            return _valueStr;
        }

        public List<KeyValuePair<string, string>> GetResultKeyValueList()
        {
            if (ResultType != InfoType.MULTI_LINE_KEY_VALUE)
            {
                throw new InvalidOperationException("Cannot call GetResultList when result is NOT InfoType.MULTI_LINE_KEY_VALUE.");
            }

            return _valueKeyValuePairList;
        }

        public List<string> GetResultList()
        {
            if (ResultType != InfoType.MULTI_LINE)
            {
                throw new InvalidOperationException("Cannot call GetResultList when result is NOT InfoType.MULTI_LINE.");
            }

            return _valueStrList;
        }

    }
}
