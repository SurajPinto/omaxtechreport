﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using static TechReport.Util.Brand;
using static System.Console;
using Microsoft.Win32;
using System.Reflection;

namespace TechReport
{
    class Util
    {
        public class Shell
        {
            [DllImport("shell32.dll")]
            private static extern bool SHGetSpecialFolderPath(IntPtr hwndOwner, StringBuilder lpszPath, int nFolder, bool fCreate);

            const int MAX_PATH = 256;
            public static string GetSpecialFolderPath(CSIDL val)
            {
                StringBuilder folderPath = new StringBuilder(MAX_PATH);
                SHGetSpecialFolderPath(IntPtr.Zero, folderPath, (int)val, false);
                return folderPath.ToString();
            }
            public enum CSIDL
            {
                CSIDL_ADMINTOOLS = 0x30,
                CSIDL_ALTSTARTUP = 0x1d,
                CSIDL_APPDATA = 0x1a,
                CSIDL_BITBUCKET = 0x0a,
                CSIDL_CDBURN_AREA = 0x3b,
                CSIDL_COMMON_ADMINTOOLS = 0x2f,
                CSIDL_COMMON_ALTSTARTUP = 0x1e,
                CSIDL_COMMON_APPDATA = 0x23,
                CSIDL_COMMON_DESKTOPDIRECTORY = 0x19,
                CSIDL_COMMON_DOCUMENTS = 0x2e,
                CSIDL_COMMON_FAVORITES = 0x1f,
                CSIDL_COMMON_MUSIC = 0x35,
                CSIDL_COMMON_OEM_LINKS = 0x3a,
                CSIDL_COMMON_PICTURES = 0x36,
                CSIDL_COMMON_PROGRAMS = 0x17,
                CSIDL_COMMON_STARTMENU = 0x16,
                CSIDL_COMMON_STARTUP = 0x18,
                CSIDL_COMMON_TEMPLATES = 0x2d,
                CSIDL_COMMON_VIDEO = 0x37,
                CSIDL_COMPUTERSNEARME = 0x3d,
                CSIDL_CONNECTIONS = 0x31,
                CSIDL_CONTROLS = 0x03,
                CSIDL_COOKIES = 0x21,
                CSIDL_DESKTOP = 0,
                CSIDL_DESKTOPDIRECTORY = 0x10,
                CSIDL_DRIVES = 0x11,
                CSIDL_FAVORITES = 0x06,
                CSIDL_FONTS = 0x14,
                CSIDL_HISTORY = 0x22,
                CSIDL_INTERNET = 0x01,
                CSIDL_INTERNET_CACHE = 0x20,
                CSIDL_LOCAL_APPDATA = 0x1c,
                CSIDL_MYDOCUMENTS = 0x0c,
                CSIDL_MYMUSIC = 13,
                CSIDL_MYPICTURES = 0x27,
                CSIDL_MYVIDEO = 0x0e,
                CSIDL_NETHOOD = 0x13,
                CSIDL_NETWORK = 0x12,
                CSIDL_PERSONAL = 0x05,
                CSIDL_PRINTERS = 0x04,
                CSIDL_PRINTHOOD = 0x1b,
                CSIDL_PROFILE = 0x28,
                CSIDL_PROGRAM_FILES = 0x26,
                CSIDL_PROGRAM_FILES_COMMON = 0x2b,
                CSIDL_PROGRAM_FILES_COMMONX86 = 0x2c,
                CSIDL_PROGRAM_FILESX86 = 0x2a,
                CSIDL_PROGRAMS = 0x02,
                CSIDL_RECENT = 0x08,
                CSIDL_RESOURCES = 0x38,
                CSIDL_RESOURCES_LOCALIZED = 0x39,
                CSIDL_SENDTO = 0x09,
                CSIDL_STARTMENU = 0x0b,
                CSIDL_STARTUP = 0x07,
                CSIDL_SYSTEM = 0x25,
                CSIDL_SYSTEMX86 = 0x29,
                CSIDL_TEMPLATES = 0x15,
                CSIDL_WINDOWS = 0x24,
                CSIDL_FLAG_CREATE = 0x8000,
                CSIDL_FLAG_DONT_EXPAND = 0x2000,
                CSIDL_FLAG_DONT_VERIFY = 0x4000,
                CSIDL_FLAG_NO_ALIAS = 0x1000,
                CSIDL_FLAG_PER_USER_INIT = 0x0800,
                CSIDL_FLAG_MASK = 0xFF00
            }
        }


        public class FilesAndFolders
        {
            public const string OMAXFolderName = @"OMAX_Corporation";
            public const string OMAXAllUserFolderName = @"AllUserData";
            public const string OMAXScriptRootFolderName = @"Scripts";
            public const string OMAXScriptFolderName = @"OMAX_Scripts";
            public const string OMAXLayoutAndMakeFolderName = @"OMAX_LAYOUT_and_MAKE";
            public const string OMAXAppFolderName = @"OMAX Corporation";
            public const string OMAXReferenceFolderName = @"OMAX_Reference";

            public const string OMAXIniLocation01 = @"";
            public const string OMAXIniLocation02 = @"FunctionKey.ini";

            private static string OMAXInstalledFolder = string.Empty;



            public static string GetOMAXSharedFileRootFolder()
            {
                string sharedFileRootFolder = string.Empty;
                string publicDir = Environment.GetEnvironmentVariable("public");
                if (string.IsNullOrEmpty(publicDir))
                    publicDir = string.Empty;
                sharedFileRootFolder = Path.Combine(publicDir, OMAXFolderName, OMAXAllUserFolderName);
                return sharedFileRootFolder;
            }

            public static string GetOMAXScriptFolder()
            {
                string scriptFolder = string.Empty;
                scriptFolder = Path.Combine(GetOMAXSharedFileRootFolder(), OMAXScriptRootFolderName, OMAXScriptFolderName);
                return scriptFolder;
            }

            public static string GetOMAXLayoutAndMakeFolder()
            {
                string omaxInstalledFolderName = string.Empty;
                if (string.IsNullOrEmpty(OMAXInstalledFolder))
                {
                    // 1st way get installed folder is from OMAX_Startup.ini file
                    string omaxStartupIni = Path.Combine(GetOMAXSharedFileRootFolder(), "OMAX_Startup.ini");
                    omaxInstalledFolderName = Util.Ini.ReadValue("Installation", "InstallDir", omaxStartupIni, string.Empty);
                    if (DirectoryContainsRequiredApplicatons(omaxInstalledFolderName))
                        OMAXInstalledFolder = omaxInstalledFolderName;


                    // 2nd way get installed folder by creating path using program files (x86) , company name and application name
                    if (string.IsNullOrEmpty(OMAXInstalledFolder))
                    {
                        string progFilesFolder = Environment.GetEnvironmentVariable("ProgramFiles(x86)");
                        omaxInstalledFolderName = Path.Combine(progFilesFolder, OMAXAppFolderName, OMAXLayoutAndMakeFolderName);
                        if (DirectoryContainsRequiredApplicatons(omaxInstalledFolderName))
                            OMAXInstalledFolder = omaxInstalledFolderName;
                    }

                    // 3rd way get installed folder by creating path using program files , company name and application name
                    if (string.IsNullOrEmpty(OMAXInstalledFolder))
                    {
                        string progFilesFolder = Environment.GetEnvironmentVariable("ProgramFiles");
                        omaxInstalledFolderName = Path.Combine(progFilesFolder, OMAXAppFolderName, OMAXLayoutAndMakeFolderName);
                        if (DirectoryContainsRequiredApplicatons(omaxInstalledFolderName))
                            OMAXInstalledFolder = omaxInstalledFolderName;
                    }

                    // 4th way get installed folder by going through all directories
                    if (string.IsNullOrEmpty(OMAXInstalledFolder))
                    {
                        omaxInstalledFolderName = GetInstallDirectory();
                        if (DirectoryContainsRequiredApplicatons(omaxInstalledFolderName))
                            OMAXInstalledFolder = omaxInstalledFolderName;
                    }
                }

                return OMAXInstalledFolder;
            }

            public static string GetLayoutPreferencesINIFilename()
            {
                string iniFilename = string.Empty;
                BrandType brandValue = GetBrand();

                switch (brandValue)
                {
                    case BrandType.B: iniFilename = "Preferences.INI"; break;
                    case BrandType.P: iniFilename = "Preferences_B.INI"; break;
                    case BrandType.H: iniFilename = "Preferences_C.INI"; break;
                    case BrandType.G: iniFilename = "Preferences_D.INI"; break;
                    default: iniFilename = "Preferences.INI"; break;
                }

                return iniFilename;
            }

            public static string GetMakePreferencesINIFilename()
            {
                string iniFilename = string.Empty;
                BrandType brandValue = GetBrand();

                switch (brandValue)
                {
                    case BrandType.B: iniFilename = "MAKEPreferences.INI"; break;
                    case BrandType.P: iniFilename = "MAKEPreferences_B.INI"; break;
                    case BrandType.H: iniFilename = "MAKEPreferences_C.INI"; break;
                    case BrandType.G: iniFilename = "MAKEPreferences_D.INI"; break;
                    default: iniFilename = "MAKEPreferences.INI"; break;
                }

                return iniFilename;
            }

            public static string GetHelpDocumentation()
            {
                string helpFilename = string.Empty;
                BrandType brandValue = GetBrand();

                switch (brandValue)
                {
                    case BrandType.B: helpFilename = ""; break;
                    case BrandType.P: helpFilename = ""; break;
                    case BrandType.H: helpFilename = ""; break;
                    case BrandType.G: helpFilename = ""; break;
                    default: helpFilename = ""; break;
                }

                return helpFilename;
            }

            private static bool DirectoryContainsRequiredApplicatons(string directory)
            {
                int count = 0;
                try
                {
                    string[] files = Directory.GetFiles(directory);
                    string[] subDirs = Directory.GetDirectories(directory);

                    List<string> filesRequiredList = new List<string>()
                    {
                        "ABOUTOMAX.EXE", "OMAX_SCRIPT.EXE", "OMAX_SENDMAIL.EXE", "OMAX_STARTUP.EXE",
                        "OMAXCALCULATOR.EXE", "SYSTEM_MONITOR.EXE", "USB_DISCONNECT.EXE", "USERSETTINGSUTILITY.EXE",
                        "WINMAKE.EXE", "LAYOUT.EXE"
                    };

                    List<string> subDirectoryRequiredList = new List<string>()
                    {
                        "AUDIO", "CURSORS", "DRIVERS", "FONTS", "HISTORY", "ICONS", "OMAXINSTALL", "RASTERGRAPHICS",
                        "USBVIEWER", "WALLPAPER"
                    };

                    int total = filesRequiredList.Count + subDirectoryRequiredList.Count;

                    foreach (string filename in files)
                    {
                        string filename1 = Path.GetFileName(filename).ToUpper();
                        if (filesRequiredList.Contains(filename1))
                            count++;

                    }

                    foreach (string subDir in subDirs)
                    {
                        string[] seps = subDir.Split(Path.DirectorySeparatorChar);
                        string directory1 = seps[seps.Length - 1].ToUpper();
                        if (subDirectoryRequiredList.Contains(directory1))
                            count++;
                    }



                    if (total == count)
                        return true;
                }
                catch (Exception ex)
                {
                    WriteLine(ex.ToString());
                }

                return false;
            }

            private static void DirSearch(string sDir, ref List<string> possiblePaths)
            {
                try
                {
                    foreach (string dir in Directory.GetDirectories(sDir))
                    {
                        string[] files;

                        try
                        {
                            files = Directory.GetFiles(dir);
                        }
                        catch (Exception) { continue; }

                        bool fileLayoutExists = false;
                        bool fileMakeExists = false;
                        foreach (string filename in files)
                        {
                            string filenameExtension = Path.GetExtension(filename);
                            if (string.Equals(".exe", filenameExtension, StringComparison.InvariantCultureIgnoreCase))
                            {
                                string filenameWithoutExtension = Path.GetFileNameWithoutExtension(filename);

                                if (string.Equals("LAYOUT", filenameWithoutExtension, StringComparison.InvariantCultureIgnoreCase))
                                    fileLayoutExists = true;

                                if (string.Equals("WinMAKE", filenameWithoutExtension, StringComparison.InvariantCultureIgnoreCase))
                                    fileMakeExists = true;
                            }
                        }

                        if (fileLayoutExists && fileMakeExists)
                            possiblePaths.Add(dir);

                        DirSearch(dir, ref possiblePaths);
                    }
                }
                catch (Exception excpt)
                {
                    throw excpt;
                }
            }
            private static string GetInstallDirectory()
            {
                string installDirectory = string.Empty;
                List<DriveInfo> hardDrives = new List<DriveInfo>();
                List<string> possiblePaths = new List<string>();

                // get all fixed drives (hard drives), ignore other types such as CD-ROM, Removable
                DriveInfo[] drives = DriveInfo.GetDrives();
                foreach (DriveInfo drive in drives)
                {
                    WriteLine(drive.DriveType);
                    switch (drive.DriveType.ToString().ToUpper())
                    {
                        case "FIXED": hardDrives.Add(drive); break;
                        default: break;
                    }
                }

                foreach (DriveInfo drive in hardDrives)
                {
                    string rootDir = drive.RootDirectory.FullName;
                    DirSearch(rootDir, ref possiblePaths);
                }

                if (possiblePaths.Count > 0)
                {
                    installDirectory = possiblePaths[0];
                }

                foreach (string str in possiblePaths)
                {
                    // C:\Program Files (x86)\OMAX Corporation\OMAX_LAYOUT_and_MAKE
                    if (str.Contains("Program Files (x86)") &&
                        str.Contains("OMAX Corporation") &&
                        str.Contains("OMAX_LAYOUT_and_MAKE") &&
                        str.Contains("C:")
                        )
                    {
                        installDirectory = str;
                        break;
                    }
                }

                if (string.IsNullOrEmpty(installDirectory))
                {
                    foreach (string str in possiblePaths)
                    {
                        if (str.Contains("Program Files") &&
                            str.Contains("OMAX Corporation") &&
                            str.Contains("OMAX_LAYOUT_and_MAKE")
                            )
                        {
                            installDirectory = str;
                            break;
                        }
                    }
                }

                return installDirectory;
            }
        }






        public class Brand
        {
            public enum BrandType { B, P, H, G, U };

            public enum BrandNames { brandOMAX, brandMAXIEM, brandHyperJET, brandGlobal, Unknown };



            /// <summary>
            /// Returns "True" if this is the "Basic" version of the software, which is
            /// determined by reading the first character of the garbage file "bop.dll"
            /// (bop.dll stands for "Basic Or Premium".)

            ///Note: This does NOT mean the software is properly registered.
            /// Use Samson_MaxVersion or Samson_DecodePassword to check if
            /// it's properly registered.

            /// Note: InstallDir is an OPTIONAL parameter to pass.If you leave it empty,
            /// then we assume the application.exe's dir is the install dir.  However
            /// it is useful to set the install dir in the case where you need to
            /// know if it's samson or not, but your exe that is wondering is in
            /// a different directory, which is the case when running as a custom
            /// extension within an installer such as Wise.

            /// "ForceToTrue" forces this routine to always report "True" in the future,
            /// even if we though we were a samson previously.This is used by Make to
            ///force this routine to always report True when connected to a Samson USB card,
            ///so that a hacker absolutely cannot simply install Premium software on a
            ///Samson, and have it work.
            /// </summary>
            /// <returns></returns>
            public static BrandType GetBrand()
            {
                BrandType brand = BrandType.B;
                string filename = Path.Combine(Util.FilesAndFolders.GetOMAXLayoutAndMakeFolder(), "bop.dll");
                byte[] fileData = File.ReadAllBytes(filename);
                char firstByteAsChar = Convert.ToChar(fileData[0]);
                switch (firstByteAsChar)
                {
                    case 'B': brand = BrandType.B; break;
                    case 'P': brand = BrandType.P; break;
                    case 'H': brand = BrandType.H; break;
                    case 'G': brand = BrandType.G; break;
                    default: brand = BrandType.U; break;
                }

                return brand;
            }

            public static string GetBrandName()
            {
                string brandName = string.Empty;
                switch (GetBrand())
                {
                    case BrandType.B: brandName = "OMAX"; break;
                    case BrandType.P: brandName = "Maxiem"; break;
                    case BrandType.H: brandName = "HyperJet"; break;
                    case BrandType.G: brandName = "Global"; break;
                    default: brandName = "UKNOWN BRANDING"; break;
                }

                return brandName;
            }
        }















        public class Ini
        {
            [DllImport("kernel32", CharSet = CharSet.Unicode)]
            private static extern int GetPrivateProfileString(string section, string key,
                string defaultValue, StringBuilder value, int size, string filePath);

            [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
            static extern int GetPrivateProfileString(string section, string key, string defaultValue,
                [In, Out] char[] value, int size, string filePath);

            [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
            private static extern int GetPrivateProfileSection(string section, IntPtr keyValue,
                int size, string filePath);

            [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool WritePrivateProfileString(string section, string key,
                string value, string filePath);



            public static int capacity = 512;



            /// <summary>
            /// REad value of key in a section of an ini file
            /// </summary>
            /// <param name="section"></param>
            /// <param name="key"></param>
            /// <param name="filePath"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static string ReadValue(string section, string key, string filePath, string defaultValue = "")
            {
                var value = new StringBuilder(capacity);
                GetPrivateProfileString(section, key, defaultValue, value, value.Capacity, filePath);
                return value.ToString();
            }

            /// <summary>
            /// Read all sections in a ini file
            /// </summary>
            /// <param name="filePath"></param>
            /// <returns></returns>
            public static string[] ReadSections(string filePath)
            {
                // first line will not recognize if ini file is saved in UTF-8 with BOM 
                while (true)
                {
                    char[] chars = new char[capacity];
                    int size = GetPrivateProfileString(null, null, "", chars, capacity, filePath);

                    if (size == 0)
                    {
                        return null;
                    }

                    if (size < capacity - 2)
                    {
                        string result = new String(chars, 0, size);
                        string[] sections = result.Split(new char[] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
                        return sections;
                    }

                    capacity = capacity * 2;
                }
            }

            /// <summary>
            /// Read all keys in a section of an ini file
            /// </summary>
            /// <param name="section"></param>
            /// <param name="filePath"></param>
            /// <returns></returns>
            public static string[] ReadKeys(string section, string filePath)
            {
                // first line will not recognize if ini file is saved in UTF-8 with BOM 
                while (true)
                {
                    char[] chars = new char[capacity];
                    int size = GetPrivateProfileString(section, null, "", chars, capacity, filePath);

                    if (size == 0)
                    {
                        return null;
                    }

                    if (size < capacity - 2)
                    {
                        string result = new String(chars, 0, size);
                        string[] keys = result.Split(new char[] { '\0' }, StringSplitOptions.RemoveEmptyEntries);
                        return keys;
                    }

                    capacity = capacity * 2;
                }
            }

            public static string[] ReadKeyValuePairs(string section, string filePath)
            {
                while (true)
                {
                    IntPtr returnedString = Marshal.AllocCoTaskMem(capacity * sizeof(char));
                    int size = GetPrivateProfileSection(section, returnedString, capacity, filePath);

                    if (size == 0)
                    {
                        Marshal.FreeCoTaskMem(returnedString);
                        return null;
                    }

                    if (size < capacity - 2)
                    {
                        string result = Marshal.PtrToStringAuto(returnedString, size - 1);
                        Marshal.FreeCoTaskMem(returnedString);
                        string[] keyValuePairs = result.Split('\0');
                        return keyValuePairs;
                    }

                    Marshal.FreeCoTaskMem(returnedString);
                    capacity = capacity * 2;
                }
            }




            /// <summary>
            /// Write a value of a key in a section
            /// </summary>
            /// <param name="section"></param>
            /// <param name="key"></param>
            /// <param name="value"></param>
            /// <param name="filePath"></param>
            /// <returns></returns>
            public static bool WriteValue(string section, string key, string value, string filePath)
            {
                bool result = WritePrivateProfileString(section, key, value, filePath);
                return result;
            }



            /// <summary>
            ///  Delete Section in a ini file
            /// </summary>
            /// <param name="section"></param>
            /// <param name="filepath"></param>
            /// <returns></returns>
            public static bool DeleteSection(string section, string filepath)
            {
                bool result = WritePrivateProfileString(section, null, null, filepath);
                return result;
            }

            /// <summary>
            /// Delete Key in a Section in a ini file
            /// </summary>
            /// <param name="section"></param>
            /// <param name="key"></param>
            /// <param name="filepath"></param>
            /// <returns></returns>
            public static bool DeleteKey(string section, string key, string filepath)
            {
                bool result = WritePrivateProfileString(section, key, null, filepath);
                return result;

            }
        }







        public class Formatting
        {
            // Return a string describing the value as a file size.
            // For example, 1.23 MB.
            public static string BytesValueStringFormattedOutputString(double value)
            {
                string[] suffixes = { "bytes", "KB", "MB", "GB",
                                        "TB", "PB", "EB", "ZB", "YB"};
                for (int i = 0; i < suffixes.Length; i++)
                {
                    if (value <= (Math.Pow(1024, i + 1)))
                    {
                        return ThreeNonZeroDigits(value /
                            Math.Pow(1024, i)) +
                            " " + suffixes[i];
                    }
                }

                return ThreeNonZeroDigits(value /
                    Math.Pow(1024, suffixes.Length - 1)) +
                    " " + suffixes[suffixes.Length - 1];
            }

            // Return the value formatted to include at most three
            // non-zero digits and at most two digits after the
            // decimal point. Examples:
            //         1
            //       123
            //        12.3
            //         1.23
            //         0.12
            private static string ThreeNonZeroDigits(double value)
            {
                if (value >= 100)
                {
                    // No digits after the decimal.
                    return value.ToString("0,0");
                }
                else if (value >= 10)
                {
                    // One digit after the decimal.
                    return value.ToString("0.0");
                }
                else
                {
                    // Two digits after the decimal.
                    return value.ToString("0.00");
                }
            }
        }












        public class Registry
        {
            public static string ReadRegistryKey64BitHive(RegistryHive hive, string regKey, string regValueKey, string defaultValue, bool throwException = false)
            {
                string regValue = defaultValue;
                try
                {
                    using (RegistryKey hklm = RegistryKey.OpenBaseKey(hive, RegistryView.Registry64))
                    using (RegistryKey key = hklm.OpenSubKey(regKey))
                    {
                        regValue = key.GetValue(regValueKey).ToString();
                    }
                }
                catch
                {
                    if (throwException == true)
                    {
                        throw;
                    }
                }

                return regValue;
            }
        }


















        public class SystemInformation
        {
            public static string GetSystemInformationPropertyValue(string propertyName)
            {
                string propertyValue = string.Empty;

                Type t = typeof(System.Windows.Forms.SystemInformation);
                PropertyInfo[] pi = t.GetProperties();
                for (int i = 0; i < pi.Length; i++)
                {
                    if (string.Equals(pi[i].Name, propertyName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        object propObjValue = pi[i].GetValue(null, null);
                        propertyValue = propObjValue.ToString();
                        break;
                    }
                }

                return propertyValue;
            }
        }

















    }
}
