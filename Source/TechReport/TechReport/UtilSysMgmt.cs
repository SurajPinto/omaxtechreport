﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Management;


namespace TechReport
{
    [Serializable]
    public class UtilSysMgmt
    {
        private static UtilSysMgmt _utilSysMgmt = null;
        private static object _syncObject = new object();
        private static Dictionary<string, List<Dictionary<string, string>>> _allResults = new Dictionary<string, List<Dictionary<string, string>>>();

        public enum SYS_MANAGEMENT
        {
            Win32_LocalTime,
            Win32_OperatingSystem,
            Win32_ComputerSystem,
            Win32_BIOS,
            Win32_DesktopMonitor,
            Win32_VideoController,
            Win32_SystemDriver,
            Win32_Processor,
            Win32_Pseudo
        }

        private static UtilSysMgmt CreateInstance()
        {
            if (_utilSysMgmt == null)
            {
                lock (_syncObject)
                {
                    if (_utilSysMgmt == null)
                    {
                        _utilSysMgmt = new UtilSysMgmt();
                        _utilSysMgmt.GetInfo();

                    }
                }
            }

            return _utilSysMgmt;
        }

        private List<Dictionary<string, string>> DeviceInformation(string stringIn)
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

            StringBuilder StringBuilder1 = new StringBuilder(string.Empty);
            try
            {
                ManagementClass ManagementClass1 = new ManagementClass(stringIn);
                ManagementObjectCollection ManagemenobjCol = ManagementClass1.GetInstances();
                PropertyDataCollection properties = ManagementClass1.Properties;

                foreach (ManagementObject obj in ManagemenobjCol)
                {
                    Dictionary<string, string> dictProperties = new Dictionary<string, string>();
                    foreach (PropertyData property in properties)
                    {
                        try
                        {
                            StringBuilder1.AppendLine(property.Name + ":  " + obj.Properties[property.Name].Value.ToString());
                            dictProperties.Add(property.Name, obj.Properties[property.Name].Value.ToString());
                        }
                        catch
                        {
                        }
                    }
                    result.Add(dictProperties);
                }
            }
            catch { }

            return result;
        }


        private void GetInfo()
        {
            for (SYS_MANAGEMENT i = SYS_MANAGEMENT.Win32_LocalTime; i < SYS_MANAGEMENT.Win32_Pseudo; i++)
            {
                List<Dictionary<string, string>> result = DeviceInformation(i.ToString());
                _allResults.Add(i.ToString(), result);
            }
        }

        public static List<Dictionary<string, string>> GetAllResults(string mgmtClassName)
        {
            if (_utilSysMgmt == null)
            {
                CreateInstance();
            }

            List<Dictionary<string, string>> result = null;
            if (_allResults.ContainsKey(mgmtClassName))
            {
                result = _allResults[mgmtClassName];
            }
            else
            {
                throw new ArgumentException("Management Class Name Not In Results");
            }

            return result;
        }

        public static string GetValueForKeyInOnlyResult(string mgmtClassName, string keyName)
        {
            if (_utilSysMgmt == null)
            {
                CreateInstance();
            }

            string value = string.Empty;
            List<Dictionary<string, string>> list = UtilSysMgmt.GetAllResults(mgmtClassName);
            if (list.Count == 1)
            {
                Dictionary<string, string> dict = list[0];
                //if (Util.SysManagement.Dictionary.DictContainsKey(dict, keyName))
                if (dict.ContainsKey(keyName))
                {
                    value = dict[keyName];
                }
                else
                {
                    throw new Exception($"Result list does not have key: {keyName}");
                }
            }
            else
            {
                throw new Exception("Result list has more than 1 item, inappropriate use of this helper method");
            }

            return value;
        }

    }
}
